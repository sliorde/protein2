import os
import logging
from typing import Optional, List
import traceback
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from torch.nn.parameter import Parameter

from arguments import Args
from data_fetcher import DataFetcher
from math_utils import random_rotation_matrix, mean_squared_distances

class AdaptiveEmbedding(nn.Module):
    def __init__(self, sz_vocab, embed_dim, embed_out_dim):
        super(AdaptiveEmbedding, self).__init__()

        self.embed_scale = embed_out_dim ** 0.5

        self.embed_layer = nn.Embedding(sz_vocab, embed_dim,padding_idx=0)

        self.with_proj = False
        if embed_out_dim != embed_dim:
            self.with_proj = True
            self.embed_proj = nn.Parameter(torch.Tensor(embed_out_dim, embed_dim))

    def forward(self, x):
        embed = self.embed_layer(x)
        if self.with_proj:
            embed  = F.linear(embed, self.embed_proj)
        embed.mul_(self.embed_scale)
        return embed

class PositionwiseFF(nn.Module):
    def __init__(self, model_dim, inner_dim, dropout, pre_lnorm=False):
        super(PositionwiseFF, self).__init__()

        self.net = nn.Sequential(
            nn.Linear(model_dim, inner_dim), nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(inner_dim, model_dim),
            nn.Dropout(dropout),
        )

        self.layer_norm = nn.LayerNorm(model_dim)

        self.pre_lnorm = pre_lnorm

    def forward(self, x):
        if self.pre_lnorm:
            core_out = self.net(self.layer_norm(x))
            output = core_out + x
        else:
            core_out = self.net(x)
            output = self.layer_norm(x + core_out)
        return output

class MultiHeadAttn(nn.Module):
    def __init__(self, input_dim, output_dim, num_heads, query_dim, dropout, pre_lnorm=False, dropatt=0):
        super(MultiHeadAttn, self).__init__()

        self.num_heads = num_heads
        self.query_dim = query_dim
        self.dropout = dropout

        self.q_net = nn.Linear(input_dim, num_heads * query_dim, bias=False)
        self.kv_net = nn.Linear(input_dim, 2 * num_heads * query_dim, bias=False)

        self.drop = nn.Dropout(dropout)
        self.dropatt = nn.Dropout(dropatt)
        self.o_net = nn.Linear(num_heads * query_dim, output_dim, bias=False)

        if input_dim != output_dim:
            self.proj_net = nn.Linear(input_dim,output_dim)
        else:
            self.proj_net = None

        self.layer_norm = nn.LayerNorm(input_dim if pre_lnorm else output_dim)

        self.scale = 1 / (query_dim ** 0.5)

        self.pre_lnorm = pre_lnorm

    def forward(self, x, attn_mask):
        if self.pre_lnorm:
            x = self.layer_norm(x)

        head_q = self.q_net(x)
        head_k, head_v = torch.chunk(self.kv_net(x), 2, -1)

        head_q = head_q.view(x.size(0), x.size(1), self.num_heads, self.query_dim)
        head_k = head_k.view(x.size(0), x.size(1), self.num_heads, self.query_dim)
        head_v = head_v.view(x.size(0), x.size(1), self.num_heads, self.query_dim)

        attn_score = torch.einsum('bind,bjnd->bijn', (head_q, head_k))

        attn_score.masked_fill_(attn_mask[:,None,:,None], -float('inf'))
        self_mask = torch.diag(attn_mask.new_ones(x.size(1)))
        attn_score.masked_fill_(self_mask[None,:,:,None], -float('inf'))

        # TODO: maybe mask self-interactions

        attn_score.mul_(self.scale)

        attn_prob = F.softmax(attn_score, dim=2)
        attn_prob = self.dropatt(attn_prob)

        attn_vec = torch.einsum('bijn,bjnd->bind', (attn_prob, head_v))
        attn_vec = attn_vec.contiguous().view(
            attn_vec.size(0), attn_vec.size(1), self.num_heads * self.query_dim)

        attn_out = self.o_net(attn_vec)
        attn_out = self.drop(attn_out)

        if self.proj_net is not None:
            x = self.proj_net(x)

        if self.pre_lnorm:
            output = x + attn_out
        else:
            output = self.layer_norm(x + attn_out)

        return output

class Block(nn.Module):
    def __init__(self, input_dim, model_dim, num_heads, query_dim, inner_dim, dropout, pre_lnorm, **kwargs):
        super(Block, self).__init__()

        self.attn = MultiHeadAttn(input_dim, model_dim, num_heads, query_dim, dropout, pre_lnorm, **kwargs)
        self.ff = PositionwiseFF(model_dim, inner_dim, dropout,pre_lnorm)

    def forward(self, x, attn_mask):

        output = self.attn(x,attn_mask)
        output = self.ff(output)

        return output

class Transformer(nn.Module):
    def __init__(self, input_dim, output_dim, num_layers, num_heads, model_dim, query_dim, inner_dim,
                 dropout, dropatt, pre_lnorm=False):
        super(Transformer, self).__init__()

        self.layers = nn.ModuleList()
        for i in range(num_layers):
            self.layers.append(
                Block(input_dim if i==0 else model_dim,model_dim,num_heads,query_dim,inner_dim,dropout,dropatt=dropatt,pre_lnorm=pre_lnorm)
            )
        if output_dim != model_dim:
            self.resize = nn.Linear(model_dim,output_dim)
        else:
            self.resize = None

    def forward(self, x, attn_mask):
        for i, layer in enumerate(self.layers):
            x = layer(x,attn_mask)
        if self.resize:
            x = self.resize(x)
        return x


class Network2(nn.Module):
    def __init__(self,args:Args):
        super(Network2,self).__init__()

        self.args = args

        self.add_embedding_ops()

        self.add_interaction_ops()

        self.add_convolution_ops()

        self.steps = Parameter(torch.tensor(0,dtype=torch.int32),requires_grad=False)
        self.examples = Parameter(torch.tensor(0,dtype=torch.int32),requires_grad=False)
        self.learning_rate = Parameter(torch.tensor(0.0),requires_grad=False)

        self.optimizer = None

        initializer = lambda m: initialize_weights(m,self.args.network2_init_type,args.initialization_scale,self.args.network2_initialization_scale_proj)
        self.apply(initializer)

        logger = logging.getLogger('protein_logger')
        logger.info('initialized network. number of parameters: {:d}'.format(self.number_of_parameters()))

    def forward(self, sequence:torch.Tensor,length:torch.Tensor,initial_coordinates:Optional[torch.Tensor]=None,mode:str='EVAL',device='cpu',repetitions:Optional[int]=None):

        if repetitions is None:
            repetitions = self.args.num_repetitions

        sequence_embedded = self.embed(sequence)

        inds_out = torch.eq(sequence,0)

        if initial_coordinates is None:
            initial_coordinates = self.get_initial_coordinates(sequence, length, inds_out,device=device)

        if (mode.upper() == 'TRAIN') and self.args.add_noise_initial_training:
            initial_coordinates = self.pertrub_coordinates(initial_coordinates)
            initial_coordinates = torch.sum(random_rotation_matrix(sequence.shape[0], self.args.num_dimensions,device=device)[:, None, :, :] * initial_coordinates[:, :,None, :], 2)
            initial_coordinates.masked_fill_(inds_out[:, :, None], self.args.aas_coordinate_padding)

        coordinates = initial_coordinates
        predicted_coordinate_list = [initial_coordinates]

        for i in range(repetitions):

            coordinates = self.one_dynamical_step(sequence_embedded, coordinates, inds_out)

            if (mode.upper() == 'TRAIN') and self.args.add_noise_in_training and (i < (repetitions - 1)) and ((i % self.args.noise_in_training_interval) == self.args.noise_in_training_interval - 1):
                coordinates = self.pertrub_coordinates(coordinates)
                coordinates.masked_fill_(inds_out[:, :, None], self.args.aas_coordinate_padding)

            predicted_coordinate_list.append(coordinates)

        # predicted_coordinates = coordinates

        return predicted_coordinate_list

    def predict_dynamics(self,sequence:torch.Tensor,length:torch.Tensor,rep:int=1,initial_coordinates:Optional[torch.Tensor]=None):
        with torch.no_grad():
            intermediate_coordinates = self(sequence,length,initial_coordinates=initial_coordinates)
            for i in range(1, rep):
                intermediate_coordinates += self(sequence,length,initial_coordinates=intermediate_coordinates[-1])[1:]
        return intermediate_coordinates

    def add_embedding_ops(self):
        self.embed = AdaptiveEmbedding(self.args.num_aas+1,self.args.aac_embedding_size,self.args.aac_embedding_size)

    def add_interaction_ops(self):
        in_size = self.args.aac_embedding_size + self.args.num_dimensions
        out_size = self.args.num_dimensions
        self.layer_norm = nn.LayerNorm(in_size)
        self.interaction = Transformer(in_size,out_size,self.args.network2_num_layers,self.args.network2_num_heads,self.args.network2_model_dim,self.args.network2_query_dim,self.args.network2_inner_dim,self.args.network2_dropout,self.args.network2_dropatt,self.args.network2_pre_lnorm)

    def add_convolution_ops(self):
        pass

    def double_data(self, sequence:torch.Tensor, length:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        sequence = sequence.repeat(2,1)
        length = length.repeat(2,1)
        true_coordinates = true_coordinates.repeat(2,1,1)
        aac_measured = aac_measured.repeat(2,1)

        return sequence,length,true_coordinates, aac_measured

    def get_initial_coordinates(self, sequence:torch.Tensor, length:torch.Tensor, inds_out:torch.Tensor, device='cpu'):
        batch_size = sequence.shape[0]

        max_length = sequence.shape[-1]

        # initial_coordinates = (torch.arange(0,max_length,device=device)[None,...].float() - (length[:real_batch_size,:].float()-1)/2)*self.args.initial_distance_between_aas*self.args.coordinates_factor

        # initial_coordinates = F.pad(initial_coordinates[...,None],[0,self.args.num_dimensions-1])

        m = 6
        k = 1.2
        t = torch.arange(0,max_length,device=device).float()
        initial_coordinates_x = ((m/(2*np.pi))*torch.cos(2*np.pi*t/m))[None,:].expand(batch_size,max_length)
        initial_coordinates_y = ((m/(2 * np.pi)) * torch.sin(2 * np.pi * t / m))[None,:].expand(batch_size,max_length)
        initial_coordinates_z = (t[None,...] - (length[:batch_size,:].float()-1)/2)/(k*m)

        initial_coordinates = torch.stack((initial_coordinates_x,initial_coordinates_y,initial_coordinates_z),dim=2)*self.args.initial_distance_between_aas*self.args.coordinates_factor

        initial_coordinates.masked_fill_(inds_out[:, :, None], self.args.aas_coordinate_padding)

        return initial_coordinates

    def pertrub_coordinates(self, coordinates:torch.Tensor):
        coordinates =  coordinates + torch.normal(torch.zeros_like(coordinates),self.args.initial_distance_between_aas*self.args.coordinates_factor*self.args.coordinate_perturbation_factor*torch.ones_like(coordinates))
        return coordinates

    def one_dynamical_step(self, sequence_embedded:torch.Tensor, coordinates:torch.Tensor, inds_out:torch.Tensor):
        all_aacs = torch.cat([sequence_embedded, coordinates], 2)  # [batch,aas,channel]
        all_aacs_normalized = self.layer_norm(all_aacs)
        interaction_forces = self.interaction(all_aacs_normalized,inds_out)
        # conv_forces = self.conv_layers(all_aacs.transpose(1, 2)).transpose_(1, 2)
        coordinates = coordinates + interaction_forces# + conv_forces
        coordinates.masked_fill_(inds_out[:,:,None],self.args.aas_coordinate_padding)

        return coordinates

    def loss(self, predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        msd = mean_squared_distances(predicted_coordinates, true_coordinates, aac_measured)
        lengths = torch.sum(aac_measured,1,keepdim=True)
        loss = torch.sum(msd*lengths.float())/(torch.sum(lengths.float())+self.args.zero_fixer)
        loss = loss / ((self.args.initial_distance_between_aas*self.args.coordinates_factor)**2)

        return loss

    def training_loss(self, coordinates_list:List[torch.Tensor], true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        aac_measured = aac_measured.long()
        training_loss = 0.0
        if not self.args.use_inermediate_gradients:
            inds = [len(coordinates_list)-1]
        else:
            inds = range(len(coordinates_list)-1,-1,-1*self.args.inermediate_gradients_interval)

        for i in inds:
            coeff = 1/(len(coordinates_list)-i)**self.args.inermediate_gradients_exponent
            training_loss = training_loss + coeff*self.loss(coordinates_list[i], true_coordinates, aac_measured)
            if self.args.neighboor_dist_coeff is not None:
                training_loss = training_loss+coeff * self.args.neighboor_dist_coeff*self.neighboor_distance_loss(coordinates_list[i], aac_measured)

        training_loss = training_loss + 0.0  # add regularization if needed

        return training_loss

    def neighboor_distance_loss(self, coordinates:torch.Tensor, aac_measured:torch.Tensor):
        diff = coordinates[:,1:,:]-coordinates[:,:(-1),:]
        squared_dists = torch.sum(diff**2,2)
        squared_dists = squared_dists.view(-1)[aac_measured[...,:(-1)].contiguous().view(-1)]
        loss = torch.sum(squared_dists - (self.args.initial_distance_between_aas * self.args.coordinates_factor)**2)
        loss = loss / ((self.args.initial_distance_between_aas*self.args.coordinates_factor)**2)
        return loss

    def evaluation_loss(self, predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor,):
        return self.loss(predicted_coordinates, true_coordinates, aac_measured)

    def add_optimizer(self):
        if self.args.optimizer.upper() == 'ADAM':
            self.optimizer = optim.Adam(self.parameters(),self.args.learning_rate)
        elif self.args.optimizer.upper() == 'MOMENTUM':
            self.optimizer = optim.SGD(self.parameters(),self.args.learning_rate,self.args.momentum,nesterov=True)
        else:
            self.optimizer = optim.SGD(self.parameters(), self.args.learning_rate, momentum=0.0, nesterov=True)

        self.scheduler = optim.lr_scheduler.ExponentialLR(self.optimizer,self.args.learning_rate_decay_rate)
        self.learning_rate.data = torch.tensor(self.scheduler.get_lr()[0])

    def number_of_parameters(self):
        return sum([p.numel() for p in self.parameters() if p.requires_grad])

    def do_train_step(self,sequence:torch.Tensor,length:torch.Tensor,true_coordinates:torch.Tensor,aac_measured:torch.Tensor,device='cpu'):

        batch_size = sequence.shape[0]

        self.train()
        self.zero_grad()

        predicted_coordinate_list = self(sequence,length,mode='TRAIN',device=device)[1:]

        training_loss = self.training_loss(predicted_coordinate_list,true_coordinates,aac_measured)
        training_loss.backward()

        if self.args.use_final_states:
            initial_coordinates = true_coordinates.detach()
            # initial_coordinates = predicted_coordinate_list[-1].detach()
            predicted_coordinate_list = self(sequence, length, initial_coordinates=initial_coordinates, mode='TRAIN', device=device,repetitions=self.args.steps_final_states)[1:]
            training_loss2 =  self.training_loss(predicted_coordinate_list,true_coordinates,aac_measured)
            (training_loss2*self.args.weight_final_states).backward()
        else:
            training_loss2 = None

        self.optimizer.step(None)
        self.steps += 1
        self.examples += batch_size

        return training_loss,training_loss2

    # def do_eval(self,sequence:torch.Tensor,length:torch.Tensor,true_coordinates:torch.Tensor,aac_measured:torch.Tensor,device='cpu'):
    #     self.eval()
    #     with torch.no_grad():
    #         predicted_coordinates = self(sequence,length,mode='EVAL',device=device)[-1]
    #         loss = self.evaluation_loss(predicted_coordinates,true_coordinates,aac_measured)
    #     return loss

    def do_eval(self,data_fetcher:DataFetcher,device='cpu'):
        self.eval()

        with torch.no_grad():
            total_loss = 0
            total_len = 0
            while total_len < data_fetcher.get_dataset_size():
                protein = data_fetcher.get_next(device)
                sequence, length, true_coordinates, aac_measured = protein.sequence, protein.length, protein.coordinates,protein.aac_measured
                total_len += int(sequence.shape[0])

                predicted_coordinates = self(sequence,length,mode='EVAL',device=device)[-1]
                total_loss += self.evaluation_loss(predicted_coordinates,true_coordinates,aac_measured)
            loss = total_loss/total_len

        return loss

    def save_checkpoint(self,filename):
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        torch.save({
            'model_state_dict': self.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict()}
            ,filename)
        logger = logging.getLogger('protein_logger')
        logger.info('saved checkpoint to {:s}'.format(filename))

    def restore_checkpoint(self,filename,also_optimizer=False,device='cpu'):
        checkpoint = torch.load(filename,map_location=device)
        self.load_state_dict(checkpoint['model_state_dict'])
        if also_optimizer:
            if self.optimizer is None:
                self.add_optimizer()
            self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.eval()
        logger = logging.getLogger('protein_logger')
        logger.info('restored from file {:s}'.format(filename))

    def train_model(self, train_data_fetcher:DataFetcher, validation_data_fetcher:DataFetcher, dir:Optional[str], device='cpu', max_epochs=None):
        logger = logging.getLogger('protein_logger')

        if self.optimizer is None:
            self.add_optimizer()

        next_epoch_start = self.examples + train_data_fetcher.get_dataset_size()

        initial_steps, initial_examples = self.steps.clone(), self.examples.clone()

        epochs = float(self.examples) / train_data_fetcher.get_dataset_size()
        initial_epochs = epochs

        next_validation_epoch = initial_epochs + self.args.test_eval_frequency

        first_iteration = True
        first_train_iteration = True
        finished_training = False
        best_loss = np.inf
        try:
            while not finished_training:
                if not first_iteration:
                    self.scheduler.step()
                    self.learning_rate.data = torch.tensor(self.scheduler.get_lr()[0])

                    while (self.examples <= next_epoch_start):

                        if max_epochs is not None and epochs > max_epochs:
                            finished_training = True
                            break

                        protein = train_data_fetcher.get_next(device)
                        sequence, length, true_coordinates, aac_measured = protein.sequence, protein.length, protein.coordinates,protein.aac_measured

                        training_loss,training_loss2 = self.do_train_step(sequence, length, true_coordinates, aac_measured,device=device)
                        epochs = float(self.examples) / train_data_fetcher.get_dataset_size()
                        if training_loss2 is None:
                            training_loss2 = -1.0

                        # train step + training eval
                        if (self.steps%self.args.train_eval_frequency == (-1%self.args.train_eval_frequency)) or first_train_iteration:
                            logger.info('steps:{:07d}({:07d}), examples:{:07d}({:07d}), epochs:{:7.2f}({:7.2f}).   training loss={:.5E}.  training loss2={:.5E}.  learning_rate={:.2E}.'.format(self.steps - initial_steps, self.steps, self.examples - initial_examples, self.examples, epochs - initial_epochs, epochs, training_loss,training_loss2,self.learning_rate))

                        first_train_iteration = False

                    next_epoch_start = self.examples + train_data_fetcher.get_dataset_size()

                # eval after epoch
                if (validation_data_fetcher is not None) and (first_iteration or (epochs >= next_validation_epoch)):
                    next_validation_epoch = epochs + self.args.test_eval_frequency

                    # protein = validation_data_fetcher.get_next(device)
                    # sequence, length, true_coordinates, aac_measured = protein.sequence, protein.length, protein.coordinates,protein.aac_measured

                    # loss = self.do_eval(sequence,length,true_coordinates,aac_measured,device=device)
                    loss = self.do_eval(validation_data_fetcher,device=device)

                    if first_iteration:
                        logger.info('initial validation loss = {:.5E}'.format(loss))
                    else:
                        logger.info('steps:{:07d}({:07d}), examples:{:07d}({:07d}), epochs:{:7.2f}({:7.2f}),  validation loss = {:.5E}\n'.format(self.steps - initial_steps, self.steps, self.examples - initial_examples, self.examples, epochs - initial_epochs, epochs, loss))

                    # save to checkpoint
                    if (not first_iteration) and (loss < best_loss) and (dir is not None):
                        best_loss = loss
                        self.save_checkpoint(os.path.join(dir, 'checkpoint.ckpt'))

                first_iteration = False

        except Exception as e:
            logger.info('ERROR.  traceback:\n{:s}'.format('      '.join(('\n' + traceback.format_exc().lstrip()).splitlines(True))))
            raise

def init_weight(weight,init_type,initialization_scale):
    if init_type.upper() == 'UNIFORM':
        nn.init.uniform_(weight, -initialization_scale, initialization_scale)
    elif init_type.upper() == 'NORMAL':
        nn.init.normal_(weight, 0.0, initialization_scale)

def init_bias(bias):
    nn.init.constant_(bias, 0.0)

def initialize_weights(module,init_type,initialization_scale,initialization_scale_proj):
    classname = module.__class__.__name__
    if classname.find('Linear') != -1:
        if hasattr(module, 'weight') and module.weight is not None:
            init_weight(module.weight,init_type,initialization_scale)
        if hasattr(module, 'bias') and module.bias is not None:
            init_bias(module.bias)
    elif classname.find('AdaptiveEmbedding') != -1:
        if hasattr(module, 'embed_proj'):
            if module.embed_proj is not None:
                nn.init.normal_(module.embed_proj, 0.0, initialization_scale_proj)
    elif classname.find('Embedding') != -1:
        if hasattr(module, 'weight'):
            init_weight(module.weight,init_type,initialization_scale)
            module.weight.data[0] = 0
    elif classname.find('LayerNorm') != -1:
        if hasattr(module, 'weight'):
            nn.init.normal_(module.weight, 1.0, initialization_scale)
        if hasattr(module, 'bias') and module.bias is not None:
            init_bias(module.bias)


