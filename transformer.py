import torch
import torch.nn as nn
from torch.nn import functional as F

class PositionwiseFF(nn.Module):
    def __init__(self, model_dim, inner_dim, dropout):
        super(PositionwiseFF, self).__init__()

        self.net = nn.Sequential(
            nn.Linear(model_dim, inner_dim),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(inner_dim, model_dim),
            nn.Dropout(dropout),
        )

        self.layer_norm = nn.LayerNorm(model_dim)

    def forward(self, x):
        core_out = self.net(x)
        output = self.layer_norm(x + core_out)
        return output

class MultiHeadAttn(nn.Module):
    def __init__(self, input_dim, output_dim, num_heads, query_dim, dropout, dropatt=0):
        super(MultiHeadAttn, self).__init__()

        self.num_heads = num_heads
        self.query_dim = query_dim
        self.dropout = dropout

        self.q_net = nn.Linear(input_dim, num_heads * query_dim, bias=False)
        self.kv_net = nn.Linear(input_dim, 2 * num_heads * query_dim, bias=False)

        self.drop = nn.Dropout(dropout)
        self.dropatt = nn.Dropout(dropatt)
        self.o_net = nn.Linear(num_heads * query_dim, output_dim, bias=False)

        if input_dim != output_dim:
            self.proj_net = nn.Linear(input_dim,output_dim)
        else:
            self.proj_net = None

        self.layer_norm = nn.LayerNorm(output_dim)

        self.scale = 1 / (query_dim ** 0.5)

    def forward(self, x, attn_mask):

        head_q = self.q_net(x)
        head_k, head_v = torch.chunk(self.kv_net(x), 2, -1)

        head_q = head_q.view(x.size(0), x.size(1), self.num_heads, self.query_dim)
        head_k = head_k.view(x.size(0), x.size(1), self.num_heads, self.query_dim)
        head_v = head_v.view(x.size(0), x.size(1), self.num_heads, self.query_dim)

        attn_score = torch.einsum('bind,bjnd->bijn', (head_q, head_k))

        attn_score.masked_fill_(attn_mask[:,None,:,None], -float('inf'))
        self_mask = torch.diag(attn_mask.new_ones(x.size(1)))
        attn_score.masked_fill_(self_mask[None,:,:,None], -float('inf'))

        attn_score.mul_(self.scale)

        attn_prob = F.softmax(attn_score, dim=2)
        attn_prob = attn_prob.masked_fill(attn_mask[:, :, None, None], 0.0)
        attn_prob = self.dropatt(attn_prob)

        attn_vec = torch.einsum('bijn,bjnd->bind', (attn_prob, head_v))
        attn_vec = attn_vec.contiguous().view(
            attn_vec.size(0), attn_vec.size(1), self.num_heads * self.query_dim)

        attn_out = self.o_net(attn_vec)
        attn_out = self.drop(attn_out)

        if self.proj_net is not None:
            x = self.proj_net(x)

        output = self.layer_norm(x + attn_out)

        return output

class Block(nn.Module):
    def __init__(self, input_dim, model_dim, num_heads, query_dim, inner_dim, dropout, **kwargs):
        super(Block, self).__init__()

        self.attn = MultiHeadAttn(input_dim, model_dim, num_heads, query_dim, dropout, **kwargs)
        self.ff = PositionwiseFF(model_dim, inner_dim, dropout)

    def forward(self, x, attn_mask):

        output = self.attn(x,attn_mask)
        output = self.ff(output)

        return output

class Transformer(nn.Module):
    def __init__(self, input_dim, output_dim, num_layers, num_heads, model_dim, query_dim, inner_dim,
                 dropout, dropatt):
        super(Transformer, self).__init__()

        self.layers = nn.ModuleList()
        for i in range(num_layers):
            self.layers.append(
                Block(input_dim if i==0 else model_dim,model_dim,num_heads,query_dim,inner_dim,dropout,dropatt=dropatt)
            )
        if output_dim != model_dim:
            self.resize = nn.Linear(model_dim,output_dim)
        else:
            self.resize = None

    def forward(self, x, attn_mask):
        for i, layer in enumerate(self.layers):
            x = layer(x,attn_mask)
        if self.resize:
            x = self.resize(x)
        return x

