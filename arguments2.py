class Args():

    # number of amino acid types
    num_aas = 20

    # number of dimensions of space
    num_dimensions = 3

    # the dataset contains the coordinates of this number of atoms per amino acid
    num_atoms_with_coordinates_in_aas = 3

    # among the 3 atoms of each amino acid, we will use the coordinates of the atom with this index (this is the C-alpha atom)
    index_of_ca = 1

    # pickle files with training data
    training_set_files = ['dataset/data/training_30.pickle', 'dataset/data/training_50.pickle', 'dataset/data/training_70.pickle', 'dataset/data/training_90.pickle','dataset/data/training_95.pickle', 'dataset/data/training_100.pickle']

    # pickle files with validation data
    validation_set_files = ['dataset/data/validation.pickle']

    # use only proteins of length smaller or equal to this length for training and evaluating
    max_protein_length = None

    # for a protein of length N, the number of operations of the forward/backward passes is of the order N^2. Therefore, we don't define the bacth size as the number of proteins, but as this effective size which should be proportionate to the sum of the squared length of the proteins.
    effective_batch_size = 300000

    # when forming a batch, not all proteins are of the same length, which means that we need to add padding to the shorter proteins. This value will be used for padding.
    aas_coordinate_padding = 0.0

    # we will initialize the protein such that this is the distance between amino acids (in pm). this number is known to be the average distance between amino acids in proteins.
    initial_distance_between_aas = 380.401552501 # pm, i think...

    # we multiply all lengths by this number to keep them in reasonable size
    coordinates_factor = 1e-5

    # coeeficient of loss term that demands that the distance between neighbors is equal to   `initial_distance_between_aas`
    neighboor_dist_coeff = 1e-1

    optimizer = 'ADAM'

    # initialize neural network weights using this scale
    initialization_scale = 1e-2

    learning_rate = 8e-4
    learning_rate_decay_rate = 1.0

    momentum = 0.9

    # small number to add to things so that we don't calculate 1/0, log(0) etc.
    zero_fixer = 1e-7

    # the amino acid type will be embedded into this number of dimensions
    aac_embedding_size = 4

    # whether to use transformer (otherwise - MLP)
    use_transformer = False

    # widths of layers of MLP
    mlp_widths = [64,64,64]

    # whether to add layernorm in MLP
    with_norm = False

    # transformer parameters
    num_layers = 4
    num_heads = 8
    model_dim = 160
    query_dim = 20
    inner_dim = 400
    dropout = 0.1
    dropatt = 0.1
    init_type = 'UNIFORM'
    initialization_scale_proj = 0.01

    # run model on initial coordinates this number of times before performing prediction or calculating the loss
    num_repetitions = 30

    # whether should add random noise to initial coordinates in training
    add_noise_initial_training = False

    # whether should add random noise to intermediate coordinates after each step in training
    add_noise_in_training = False

    # once every how many steps to add noise to intermediate coordinates in training
    noise_in_training_interval = 5

    # the noise that will be added to coordinates during training will have std of be this number times `initial_distance_between_aas`
    coordinate_perturbation_factor = 0.02

    # whether to add to the training batch also examples whose initial coordinates are the true coordinates, to encourage the stability of the final configuration
    use_final_states = False

    # coefficient of loss term of proteins whose initial coordinates are taken as the true coordinates
    weight_final_states = 0.2

    # for proteins whose initial coordinates are taken as the true coordinates, do this number of iterations of the model (instead of `num_repetitions`)
    steps_final_states = 1

    # whether to calculate loss only based on the final coordinates or also using intermediate coordinates
    use_inermediate_gradients = False

    # if `use_inermediate_gradients==True`, calculate loss of intermediate coordinates once every this number of steps
    inermediate_gradients_interval = 3

    # if `use_inermediate_gradients==True`, the coeeficient of the  loss of intermediate coordinates will decrease as the number of steps from the last step raised to this power
    inermediate_gradients_exponent = 2

    # for random number generation
    seed = 2929

    # how many train steps between train messages
    train_eval_frequency = 5

    # how many epochs between doing evaluation on test set
    test_eval_frequency = 100

    def __str__(self):
        s = ''
        for att in self.__dir__():
            if '__' not in att:
                s += '{:s}={:s}\n'.format(att,getattr(self,att).__repr__())
        return s

    def __format__(self, format_spec):
        return self.__str__()

args = Args()
