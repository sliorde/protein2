import pickle
from glob import glob
import random
import os
from collections import namedtuple
import numpy as np
import tensorflow as tf
import torch

# from utils import Align
from arguments import Args
from protein import Protein

class DataFetcher():
    def __init__(self, mode: str, args: Args, repeat: bool = True, shuffle: bool = None,
                 use_preprocessed: bool = True):

        data_format = 'NWC'

        if mode == 'TRAIN':
            batch_size = args.batch_size
            if use_preprocessed:
                path = args.preprocessed_training_set_path
            else:
                path = args.training_set_path
            size = calc_size(args.max_protein_length, args.training_lengths, args.training_accum_examples)
            shuffle = True if shuffle is None else shuffle
        elif mode == 'EVAL':
            if use_preprocessed:
                path = args.preprocessed_validation_set_path
            else:
                path = args.validation_set_path
            size = calc_size(args.max_protein_length, args.validation_lengths, args.validation_accum_examples)
            batch_size = size
            shuffle = False if shuffle is None else shuffle
        elif mode == 'FAKE':
            batch_size = args.batch_size
            path = None
            size = args.fake_dataset_size
            shuffle = False if shuffle is None else shuffle
            max_possible_length = 500 if args.max_protein_length is None else args.max_protein_length
            min_possible_length = 100 if args.max_protein_length is None else np.minimum(100, args.max_protein_length)
            known_if_aac_measured_prob = 0.95
            aac_measured_prob = 0.95
        else:
            raise ValueError("mode needs to be one of 'TRAIN','EVAL','FAKE'")

        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph,config=tf.ConfigProto(device_count={'GPU': 0}))

        if (not mode == 'FAKE') and (not use_preprocessed):
            if os.path.isdir(path):
                file_names = glob(path + "/**/*", recursive=True)
                file_names = [f for f in file_names if not os.path.isdir(f)]
                if shuffle:
                    random.shuffle(file_names)
                else:
                    file_names = sorted(file_names)
            else:
                file_names = [path]

        with self.graph.as_default():
            if (not mode == 'FAKE') and (not use_preprocessed):
                dataset = tf.data.TFRecordDataset(file_names, num_parallel_reads=args.num_threads_read_file)
                if shuffle:
                    dataset = dataset.shuffle(args.shuffle_buffer_size)

                if (batch_size is None):
                    if len(file_names) == 1:
                        batch_size = sum(1 for _ in tf.python_io.tf_record_iterator(file_names[0]))
                    else:
                        batch_size = 1

                def parse_function(serialized_example):
                    context, features = tf.parse_single_sequence_example(serialized_example,
                                                                         context_features={
                                                                             'id': tf.FixedLenFeature((1,), tf.string)},
                                                                         sequence_features={
                                                                             'primary': tf.FixedLenSequenceFeature((1,),
                                                                                                                   tf.int64),
                                                                             # 'evolutionary': tf.FixedLenSequenceFeature((NUM_AAS+1,), tf.float32,allow_missing=True),
                                                                             # 'secondary': tf.FixedLenSequenceFeature((1,),tf.int64,allow_missing=True),
                                                                             'tertiary': tf.FixedLenSequenceFeature(
                                                                                 (args.num_dimensions,), tf.float32,
                                                                                 allow_missing=True),
                                                                             'mask': tf.FixedLenSequenceFeature((1,),
                                                                                                                tf.float32,
                                                                                                                allow_missing=True)})

                    id = context['id']
                    # sequence = tf.to_int32(features['primary'][:, 0] + 1)
                    sequence = features['primary'][:, 0] + 1
                    length = tf.expand_dims(tf.size(sequence), 0)
                    coordinates = features['tertiary'][args.index_of_ca::args.num_atoms_with_coordinates_in_aas,
                                  :] * args.coordinates_factor
                    aac_measured = tf.to_int32(features['mask'][:, 0])
                    known_if_aac_measured = tf.expand_dims(tf.not_equal(tf.size(aac_measured), 0), 0)
                    aac_measured = tf.cond(known_if_aac_measured[0], lambda: aac_measured,
                                           lambda: tf.zeros_like(known_if_aac_measured, tf.int32))

                    return id, sequence, length, coordinates, aac_measured, known_if_aac_measured

                dataset = dataset.map(parse_function, args.num_threads_preprocessing)

                if args.max_protein_length is not None:
                    dataset = dataset.filter(
                        lambda id, sequence, length, coordinates, aac_measured, known_if_aac_measured:
                        tf.less_equal(tf.squeeze(length), args.max_protein_length))

                # def align_protein(id, sequence, length, coordinates, aac_measured, known_if_aac_measured):
                #     coordinates = Align(coordinates, aac_measured)
                #     return id, sequence, length, coordinates, aac_measured, known_if_aac_measured
                # dataset = dataset.map(align_protein,args.num_threads_preprocessing)

                if data_format == 'NCW':
                    def transpose(id, sequence, length, coordinates, aac_measured, known_if_aac_measured):
                        coordinates = tf.transpose(coordinates)
                        return id, sequence, length, coordinates, aac_measured, known_if_aac_measured

                    dataset = dataset.map(transpose, args.num_threads_preprocessing)
            elif (not mode == 'FAKE') and use_preprocessed:
                with open(path, 'rb') as f:
                    data = pickle.load(f)

                def generateNext():
                    for i in range(len(data.id)):
                        yield (data.id[i], data.sequence[i], data.length[i], data.coordinates[i], data.aac_measured[i],
                               data.known_if_aac_measured[i])

                dataset = tf.data.Dataset.from_generator(generateNext,
                                                         (tf.string, tf.int64, tf.int32, tf.float32, tf.int32, tf.bool))
                if shuffle:
                    dataset = dataset.shuffle(args.shuffle_buffer_size)
                if (batch_size is None):
                    batch_size = len(data.id)
                if args.max_protein_length is not None:
                    dataset = dataset.filter(
                        lambda id, sequence, length, coordinates, aac_measured, known_if_aac_measured:
                        tf.less_equal(tf.squeeze(length), args.max_protein_length))
                if data_format == 'NCW':
                    def transpose(id, sequence, length, coordinates, aac_measured, known_if_aac_measured):
                        coordinates = tf.transpose(coordinates)
                        return id, sequence, length, coordinates, aac_measured, known_if_aac_measured

                    dataset = dataset.map(transpose, args.num_threads_preprocessing)
            else:
                def generate_next():
                    for i in range(size):
                        id = np.array(['FAKE' + str(i)])
                        length = np.random.randint(min_possible_length, max_possible_length + 1, [1])
                        sequence = np.random.randint(1, args.num_aas + 1, length[0], np.int64)
                        known_if_aac_measured = np.random.uniform(size=(1,)) < known_if_aac_measured_prob
                        if known_if_aac_measured:
                            aac_measured = (np.random.uniform(size=length[0]) < aac_measured_prob).astype(np.int32)
                        else:
                            aac_measured = (np.zeros(length[0], np.int32))
                        coordinates = np.cumsum(
                            args.initial_distance_between_aas * args.coordinates_factor * np.random.normal(
                                size=(length[0], args.num_dimensions)), 0).astype(np.float32)
                        if data_format == 'NCW':
                            coordinates = tf.transpose(coordinates)
                        yield (id, sequence, length, coordinates, aac_measured, known_if_aac_measured)

                dataset = tf.data.Dataset.from_generator(generate_next,
                                                         (tf.string, tf.int64, tf.int32, tf.float32, tf.int32, tf.bool))

            if repeat:
                dataset = dataset.repeat()

            if data_format == 'NWC':
                dataset = dataset.padded_batch(batch_size, padded_shapes=(
                (1,), (-1,), (1,), (-1, args.num_dimensions), (-1,), (1,)),
                                               padding_values=(' ', tf.constant(0,tf.int64), 0, args.aas_coordinate_padding, 0, True))
            else:  # 'NCW'
                dataset = dataset.padded_batch(batch_size, padded_shapes=(
                (1,), (-1,), (1,), (args.num_dimensions, -1), (-1,), (1,)),
                                               padding_values=(' ', tf.constant(0,tf.int64), 0, args.aas_coordinate_padding, 0, True))

            self._next = dataset.make_one_shot_iterator().get_next()

        self.dataset_size = size

    def get_next(self,device='cpu'):
        id, sequence, length, coordinates, aac_measured, known_if_aac_measured = self.sess.run(self._next)
        sequence = torch.from_numpy(sequence).to(device)
        length = torch.from_numpy(length).to(device)
        coordinates = torch.from_numpy(coordinates).to(device)
        aac_measured = torch.from_numpy(aac_measured).to(device)
        known_if_aac_measured = torch.from_numpy(known_if_aac_measured.astype(np.uint8)).to(device)
        protein = Protein(id, sequence, length, coordinates, aac_measured, known_if_aac_measured)
        return protein

    def get_dataset_size(self):
        return self.dataset_size


def calc_size(max_protein_length, lengths, accum_examples):
    if max_protein_length is None:
        return accum_examples[-1]
    else:
        if max_protein_length >= lengths[-1]:
            return accum_examples[-1]
        else:
            ind = np.nonzero(np.greater(lengths, max_protein_length))[0][0]
            if ind == 0:
                raise ValueError('max_protein_length too short.')
            else:
                return accum_examples[ind - 1]

