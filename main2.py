import os
import sys

import cProfile

import torch

from arguments2 import args
from dataset.protein_dataset import get_protein_dataset_and_dataiterator, data_files
from network3 import Network3
from utils import startup, log_args, set_random_seeds, save_args_to_pickle, load_args_from_pickle,update_args_from_sys_argv

output_dir = startup(__file__, __name__)

PROFILE = False
PROFILER_EPOCHS = 1

RESTORE_CHECKPOINT = None
ALSO_RESTORE_ARGUMENTS = True
ALSO_RESTORE_OPTIMIZER = True
DO_VALIDATION = True

WHICH = 'NETWORK3'

if (RESTORE_CHECKPOINT is not None) and ALSO_RESTORE_ARGUMENTS:
    args = load_args_from_pickle(os.path.join(os.path.dirname(RESTORE_CHECKPOINT),'arguments.pickle'))

args.seed = 888

args.max_protein_length = 27
args.training_set_files = args.validation_set_files

args.effective_batch_size = 60000

args.aac_embedding_size = 4

args.use_transformer = False

args.mlp_widths = [64,64,32]
args.with_norm = True

args.num_layers = 4
args.num_heads = 16
args.model_dim = 160
args.query_dim = 32
args.inner_dim = 400
args.dropout = 0.02
args.dropatt = 0
args.init_type = 'UNIFORM'
args.initialization_scale_proj = 1.0

args.num_repetitions = 30

args.add_noise_initial_training = True # True
args.add_noise_in_training = False
args.noise_in_training_interval = 1
args.coordinate_perturbation_factor = 0

args.use_final_states = False
args.weight_final_states = 0.5
args.steps_final_states = 1

args.use_inermediate_gradients = True
args.inermediate_gradients_interval = 15
args.inermediate_gradients_exponent = 0.2

args.neighboor_dist_coeff = None

args.optimizer = 'ADAM'
args.learning_rate = 1e-4
args.initialization_scale = 2e-2 # 2e-2 for transformer?

args.train_eval_frequency = 20
args.test_eval_frequency = 1  #1

update_args_from_sys_argv(args,sys.argv)

log_args(WHICH=WHICH, args=args, RESTORE_CHECKPOINT=RESTORE_CHECKPOINT, ALSO_RESTORE_ARGUMENTS=ALSO_RESTORE_ARGUMENTS, ALSO_RESTORE_OPTIMIZER=ALSO_RESTORE_OPTIMIZER,DO_VALIDATION=DO_VALIDATION)

save_args_to_pickle(os.path.join(output_dir, 'arguments.pickle'),args)

set_random_seeds(args.seed)

_,train_iterator = get_protein_dataset_and_dataiterator(args.training_set_files, args.max_protein_length, coordinate_padding=args.aas_coordinate_padding, batch_size=args.effective_batch_size)
if DO_VALIDATION:
    _,validation_iterator = get_protein_dataset_and_dataiterator(args.validation_set_files, args.max_protein_length,
                                                               coordinate_padding=args.aas_coordinate_padding,shuffle=False,batch_size=args.effective_batch_size)
else:
    validation_iterator = None

if WHICH.upper()=='NETWORK3':
    net = Network3(args)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

if RESTORE_CHECKPOINT is not None:
    net.restore_checkpoint(RESTORE_CHECKPOINT,ALSO_RESTORE_OPTIMIZER,device)

net.to(device)
net.replicate_self(torch.cuda.device_count())

if PROFILE:
    cProfile.run(
        'net.train_model(train_iterator, validation_iterator=None, dir=None, device=device,max_epochs=PROFILER_EPOCHS)',
        'profiler_stats')
else:
    net.train_model(train_iterator, validation_iterator, output_dir, device)