import os
import logging
from typing import Optional, List
import traceback
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from torch.nn.parameter import Parameter

from arguments import Args
from data_fetcher import DataFetcher
from math_utils import random_rotation_matrix, mean_squared_distances

class Network(nn.Module):
    def __init__(self,args:Args):
        super(Network,self).__init__()

        self.args = args

        self.add_embedding_ops()

        self.add_interaction_ops()

        self.add_convolution_ops()

        self.steps = Parameter(torch.tensor(0,dtype=torch.int32),requires_grad=False)
        self.examples = Parameter(torch.tensor(0,dtype=torch.int32),requires_grad=False)
        self.learning_rate = Parameter(torch.tensor(0.0),requires_grad=False)

        self.optimizer = None

        logger = logging.getLogger('protein_logger')
        logger.info('initialized network. number of parameters: {:d}'.format(self.number_of_parameters()))

    def forward(self, sequence:torch.Tensor,length:torch.Tensor,initial_coordinates:Optional[torch.Tensor]=None,mode:str='EVAL',device='cpu',repetitions:Optional[int]=None):

        if repetitions is None:
            repetitions = self.args.num_repetitions

        sequence_embedded = self.embed(sequence)

        inds_out = torch.eq(sequence,0)

        if initial_coordinates is None:
            initial_coordinates = self.get_initial_coordinates(sequence, length, inds_out,device=device)

        if (mode.upper() == 'TRAIN') and self.args.add_noise_initial_training:
            initial_coordinates = self.pertrub_coordinates(initial_coordinates)
            initial_coordinates = torch.sum(random_rotation_matrix(sequence.shape[0], self.args.num_dimensions,device=device)[:, None, :, :] * initial_coordinates[:, :,None, :], 2)
            initial_coordinates[inds_out] = self.args.aas_coordinate_padding

        coordinates = initial_coordinates
        predicted_coordinate_list = [initial_coordinates]

        for i in range(repetitions):

            coordinates = self.one_dynamical_step(sequence_embedded, coordinates, inds_out)

            if (mode.upper() == 'TRAIN') and self.args.add_noise_in_training and (i < (repetitions - 1)) and ((i % self.args.noise_in_training_interval) == self.args.noise_in_training_interval - 1):
                coordinates = self.pertrub_coordinates(coordinates)
                coordinates[inds_out] = self.args.aas_coordinate_padding

            predicted_coordinate_list.append(coordinates)

        # predicted_coordinates = coordinates

        return predicted_coordinate_list

    def predict_dynamics(self,sequence:torch.Tensor,length:torch.Tensor,rep:int=1,initial_coordinates:Optional[torch.Tensor]=None):
        with torch.no_grad():
            intermediate_coordinates = self(sequence,length,initial_coordinates=initial_coordinates)
            for i in range(1, rep):
                intermediate_coordinates += self(sequence,length,initial_coordinates=intermediate_coordinates[-1])[1:]
        return intermediate_coordinates

    def add_embedding_ops(self):
        self.embed = nn.Embedding(self.args.num_aas+1,self.args.aac_embedding_size,padding_idx=0,scale_grad_by_freq=False)
        nn.init.normal_(self.embed.weight, 0.0, self.args.initialization_scale)

    def add_interaction_ops(self):
        interaction_in_size = self.args.aac_embedding_size + self.args.num_dimensions
        interaction_out_size = self.args.num_dimensions
        interaction_hidden_layer_sizes = self.args.network1_interaction_hidden_layer_sizes
        interaction_num_layers = len(interaction_hidden_layer_sizes)
        interaction_layers = []
        for i in range(interaction_num_layers+1):
            if i==0:
                in_size = interaction_in_size
            else:
                in_size = interaction_hidden_layer_sizes[i - 1]
            if i == interaction_num_layers:
                out_size = interaction_out_size  # for attention, the last dimension here should be 1
            else:
                out_size = interaction_hidden_layer_sizes[i]
            linear_layer = nn.Linear(in_size,out_size,bias=True)
            nn.init.normal_(linear_layer.weight, 0.0, self.args.initialization_scale)
            nn.init.constant_(linear_layer.bias, self.args.initialization_scale)
            if i == interaction_num_layers:
                interaction_layers.append(linear_layer)
            else:
                interaction_layers.append(nn.Sequential(linear_layer,nn.ReLU()))
        self.interaction_layers = nn.Sequential(*interaction_layers)

    def add_convolution_ops(self):
        conv_in_size = self.args.aac_embedding_size + self.args.num_dimensions
        conv_out_size = self.args.num_dimensions
        conv_hidden_layer_sizes = self.args.network1_neighbor_hidden_layer_sizes
        conv_num_layers = len(conv_hidden_layer_sizes)
        conv_filter_sizes = self.args.network1_neighbor_filter_sizes
        conv_layers = []
        for i in range(conv_num_layers + 1):
            if i == 0:
                in_size = conv_in_size
            else:
                in_size = conv_hidden_layer_sizes[i - 1]
            if i == conv_num_layers:
                out_size = conv_out_size
            else:
                out_size = conv_hidden_layer_sizes[i]
            conv_layer = nn.Conv1d(in_size, out_size, conv_filter_sizes[i], bias=True,padding=(conv_filter_sizes[i]-1)//2)
            nn.init.normal_(conv_layer.weight, 0.0, self.args.initialization_scale)
            nn.init.constant_(conv_layer.bias, self.args.initialization_scale)
            if i == conv_num_layers:
                conv_layers.append(conv_layer)
            else:
                conv_layers.append(nn.Sequential(conv_layer, nn.ReLU()))
        self.conv_layers = nn.Sequential(*conv_layers)

    def double_data(self, sequence:torch.Tensor, length:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        sequence = sequence.repeat(2,1)
        length = length.repeat(2,1)
        true_coordinates = true_coordinates.repeat(2,1,1)
        aac_measured = aac_measured.repeat(2,1)

        return sequence,length,true_coordinates, aac_measured

    def get_initial_coordinates(self, sequence:torch.Tensor, length:torch.Tensor, inds_out:torch.Tensor, device='cpu'):
        batch_size = sequence.shape[0]
        real_batch_size = batch_size

        max_length = sequence.shape[-1]

        initial_coordinates = (torch.arange(0,max_length,device=device)[None,...].float() - (length[:real_batch_size,:].float()-1)/2)*self.args.initial_distance_between_aas*self.args.coordinates_factor

        initial_coordinates = F.pad(initial_coordinates[...,None],[0,self.args.num_dimensions-1])

        initial_coordinates[inds_out] = self.args.aas_coordinate_padding

        return initial_coordinates

    def pertrub_coordinates(self, coordinates:torch.Tensor):
        coordinates =  coordinates + torch.normal(torch.zeros_like(coordinates),self.args.initial_distance_between_aas*self.args.coordinates_factor*self.args.coordinate_perturbation_factor*torch.ones_like(coordinates))
        return coordinates

    def one_dynamical_step(self, sequence_embedded:torch.Tensor, coordinates:torch.Tensor, inds_out:torch.Tensor):
        all_aacs = torch.cat([sequence_embedded, coordinates], 2)  # [batch,aas,channel]
        interaction_forces = []
        for j in range(all_aacs.shape[1]):
            current_aac = all_aacs[:, [j], :]
            all_forces_on_current_aac = self.interaction_layers(current_aac - all_aacs)
            total_force = torch.sum(all_forces_on_current_aac, 1, keepdim=True)
            interaction_forces.append(total_force)
        interaction_forces = torch.cat(interaction_forces, 1)
        conv_forces = self.conv_layers(all_aacs.transpose(1, 2)).transpose_(1, 2)

        coordinates = coordinates + interaction_forces + conv_forces

        coordinates[inds_out] = self.args.aas_coordinate_padding

        return coordinates

    def loss(self, predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        msd = mean_squared_distances(predicted_coordinates, true_coordinates, aac_measured)
        lengths = torch.sum(aac_measured,1,keepdim=True)
        loss = torch.sum(msd*lengths.float())/(torch.sum(lengths.float())+self.args.zero_fixer)
        loss = loss / ((self.args.initial_distance_between_aas*self.args.coordinates_factor)**2)

        return loss

    def training_loss(self, coordinates_list:List[torch.Tensor], true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        aac_measured = aac_measured.long()
        training_loss = 0.0
        if not self.args.use_inermediate_gradients:
            inds = [len(coordinates_list)-1]
        else:
            inds = range(len(coordinates_list)-1,-1,-1*self.args.inermediate_gradients_interval)

        for i in inds:
            coeff = 1/(len(coordinates_list)-i)**self.args.inermediate_gradients_exponent
            training_loss = training_loss + coeff*self.loss(coordinates_list[i], true_coordinates, aac_measured)
            if self.args.neighboor_dist_coeff is not None:
                training_loss = training_loss+coeff * self.args.neighboor_dist_coeff*self.neighboor_distance_loss(coordinates_list[i], aac_measured)

        training_loss = training_loss + 0.0  # add regularization if needed

        return training_loss

    def neighboor_distance_loss(self, coordinates:torch.Tensor, aac_measured:torch.Tensor):
        diff = coordinates[:,1:,:]-coordinates[:,:(-1),:]
        squared_dists = torch.sum(diff**2,2)
        squared_dists = squared_dists.view(-1)[aac_measured[...,:(-1)].contiguous().view(-1)]
        loss = torch.sum(squared_dists - (self.args.initial_distance_between_aas * self.args.coordinates_factor)**2)
        loss = loss / ((self.args.initial_distance_between_aas*self.args.coordinates_factor)**2)
        return loss

    def evaluation_loss(self, predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor,):
        return self.loss(predicted_coordinates, true_coordinates, aac_measured)

    def add_optimizer(self):
        if self.args.optimizer.upper() == 'ADAM':
            self.optimizer = optim.Adam(self.parameters(),self.args.learning_rate)
        elif self.args.optimizer.upper() == 'MOMENTUM':
            self.optimizer = optim.SGD(self.parameters(),self.args.learning_rate,self.args.momentum,nesterov=True)
        else:
            self.optimizer = optim.SGD(self.parameters(), self.args.learning_rate, momentum=0.0, nesterov=True)

        self.scheduler = optim.lr_scheduler.ExponentialLR(self.optimizer,self.args.learning_rate_decay_rate)
        self.learning_rate.data = torch.tensor(self.scheduler.get_lr()[0])

    def number_of_parameters(self):
        return sum([p.numel() for p in self.parameters() if p.requires_grad])

    def do_train_step(self,sequence:torch.Tensor,length:torch.Tensor,true_coordinates:torch.Tensor,aac_measured:torch.Tensor,device='cpu'):

        batch_size = sequence.shape[0]

        self.train()
        self.zero_grad()

        predicted_coordinate_list = self(sequence,length,mode='TRAIN',device=device)[1:]

        training_loss = self.training_loss(predicted_coordinate_list,true_coordinates,aac_measured)
        training_loss.backward()

        if self.args.use_final_states:
            initial_coordinates = true_coordinates.detach()
            # initial_coordinates = predicted_coordinate_list[-1].detach()
            predicted_coordinate_list = self(sequence, length, initial_coordinates=initial_coordinates, mode='TRAIN', device=device,repetitions=self.args.steps_final_states)[1:]
            training_loss2 =  self.training_loss(predicted_coordinate_list,true_coordinates,aac_measured)
            (training_loss2*self.args.weight_final_states).backward()
        else:
            training_loss2 = None

        self.optimizer.step(None)
        self.steps += 1
        self.examples += batch_size

        return training_loss,training_loss2

    # def do_eval(self,sequence:torch.Tensor,length:torch.Tensor,true_coordinates:torch.Tensor,aac_measured:torch.Tensor,device='cpu'):
    #     self.eval()
    #     with torch.no_grad():
    #         predicted_coordinates = self(sequence,length,mode='EVAL',device=device)[-1]
    #         loss = self.evaluation_loss(predicted_coordinates,true_coordinates,aac_measured)
    #     return loss

    def do_eval(self,data_fetcher:DataFetcher,device='cpu'):
        self.eval()

        with torch.no_grad():
            total_loss = 0
            total_len = 0
            while total_len < data_fetcher.get_dataset_size():
                protein = data_fetcher.get_next(device)
                sequence, length, true_coordinates, aac_measured = protein.sequence, protein.length, protein.coordinates,protein.aac_measured
                total_len += int(sequence.shape[0])

                predicted_coordinates = self(sequence,length,mode='EVAL',device=device)[-1]
                total_loss += self.evaluation_loss(predicted_coordinates,true_coordinates,aac_measured)
            loss = total_loss/total_len

        return loss

    def save_checkpoint(self,filename):
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        torch.save({
            'model_state_dict': self.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict()}
            ,filename)
        logger = logging.getLogger('protein_logger')
        logger.info('saved checkpoint to {:s}'.format(filename))

    def restore_checkpoint(self,filename,also_optimizer=False):
        checkpoint = torch.load(filename)
        self.load_state_dict(checkpoint['model_state_dict'])
        if also_optimizer:
            if self.optimizer is None:
                self.add_optimizer()
            self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.eval()
        logger = logging.getLogger('protein_logger')
        logger.info('restored from file {:s}'.format(filename))

    def train_model(self, train_data_fetcher:DataFetcher, validation_data_fetcher:DataFetcher, dir:Optional[str], device='cpu', max_epochs=None):
        logger = logging.getLogger('protein_logger')

        if self.optimizer is None:
            self.add_optimizer()

        next_epoch_start = self.examples + train_data_fetcher.get_dataset_size()

        initial_steps, initial_examples = self.steps.clone(), self.examples.clone()

        epochs = float(self.examples) / train_data_fetcher.get_dataset_size()
        initial_epochs = epochs

        next_validation_epoch = initial_epochs + self.args.test_eval_frequency

        first_iteration = True
        first_train_iteration = True
        finished_training = False
        best_loss = np.inf
        try:
            while not finished_training:
                if not first_iteration:
                    self.scheduler.step()
                    self.learning_rate.data = torch.tensor(self.scheduler.get_lr()[0])

                    while (self.examples <= next_epoch_start):

                        if max_epochs is not None and epochs > max_epochs:
                            finished_training = True
                            break

                        protein = train_data_fetcher.get_next(device)
                        sequence, length, true_coordinates, aac_measured = protein.sequence, protein.length, protein.coordinates,protein.aac_measured

                        training_loss,training_loss2 = self.do_train_step(sequence, length, true_coordinates, aac_measured,device=device)
                        epochs = float(self.examples) / train_data_fetcher.get_dataset_size()
                        if training_loss2 is None:
                            training_loss2 = -1.0

                        # train step + training eval
                        if (self.steps%self.args.train_eval_frequency == (-1%self.args.train_eval_frequency)) or first_train_iteration:
                            logger.info('steps:{:07d}({:07d}), examples:{:07d}({:07d}), epochs:{:7.2f}({:7.2f}).   training loss={:.5E}.  training loss2={:.5E}.  learning_rate={:.2E}.'.format(self.steps - initial_steps, self.steps, self.examples - initial_examples, self.examples, epochs - initial_epochs, epochs, training_loss,training_loss2,self.learning_rate))

                        first_train_iteration = False

                    next_epoch_start = self.examples + train_data_fetcher.get_dataset_size()

                # eval after epoch
                if (validation_data_fetcher is not None) and (first_iteration or (epochs >= next_validation_epoch)):
                    next_validation_epoch = epochs + self.args.test_eval_frequency

                    # protein = validation_data_fetcher.get_next(device)
                    # sequence, length, true_coordinates, aac_measured = protein.sequence, protein.length, protein.coordinates,protein.aac_measured

                    # loss = self.do_eval(sequence,length,true_coordinates,aac_measured,device=device)
                    loss = self.do_eval(validation_data_fetcher,device=device)

                    if first_iteration:
                        logger.info('initial validation loss = {:.5E}'.format(loss))
                    else:
                        logger.info('steps:{:07d}({:07d}), examples:{:07d}({:07d}), epochs:{:7.2f}({:7.2f}),  validation loss = {:.5E}\n'.format(self.steps - initial_steps, self.steps, self.examples - initial_examples, self.examples, epochs - initial_epochs, epochs, loss))

                    # save to checkpoint
                    if (not first_iteration) and (loss < best_loss) and (dir is not None):
                        best_loss = loss
                        self.save_checkpoint(os.path.join(dir, 'checkpoint.ckpt'))

                first_iteration = False

        except Exception as e:
            logger.info('ERROR.  traceback:\n{:s}'.format('      '.join(('\n' + traceback.format_exc().lstrip()).splitlines(True))))
            raise



