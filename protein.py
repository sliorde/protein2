from collections import namedtuple

class Protein(
    namedtuple("Protein", ('id', 'sequence', 'length', 'coordinates', 'aac_measured', 'known_if_aac_measured'))):
    pass