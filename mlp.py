import math

import torch
from torch import nn

class MlpInteraction(nn.Module):
    def __init__(self,aac_embedding_size,widths,with_norm):
        """
        this class represents a the model for a single interaction between 2 amino acids
        Args:
            aac_embedding_size:
            widths: layer widths
            with_norm:
        """
        nn.Module.__init__(self)

        widths = [1+2*aac_embedding_size+1] + widths + [1]
        layers = []
        for i,(w1,w2) in enumerate(zip(widths[:-1],widths[1:])):
            layers.append(nn.Linear(w1,w2))
            if i < len(widths)-2:
                if with_norm:
                    layers.append(nn.LayerNorm(w2))
                layers.append(nn.ReLU())
        self.layers = nn.Sequential(*layers)

    def initialize_weights(self,scale):
        def reset_parameters(module):
            classname = module.__class__.__name__
            if classname.find('Linear') != -1:
                if hasattr(module, 'weight') and module.weight is not None:
                    fan = nn.init._calculate_correct_fan(module.weight, 'fan_in')
                    gain = nn.init.calculate_gain('relu', 0)
                    std = gain / math.sqrt(fan)
                    bound = math.sqrt(3.0) * std
                    with torch.no_grad():
                        return module.weight.uniform_(-bound*scale, bound*scale)
                if hasattr(module, 'bias') and module.bias is not None:
                    fan_in, _ = nn.init._calculate_fan_in_and_fan_out(module.bias)
                    bound = 1 / math.sqrt(fan_in)
                    nn.init.uniform_(module.bias, -bound*scale, bound*scale)
        self.apply(reset_parameters)

    def forward(self, coordinates, aacs, mask):

        dist_diffs = coordinates[:, :, None, :] - coordinates[:, None, :, :]
        square_dists = torch.sum(dist_diffs**2,-1)
        are_neighbors = torch.zeros_like(square_dists)
        are_neighbors[:, range(0, are_neighbors.shape[1] - 1), range(1, are_neighbors.shape[1])] = 1
        are_neighbors[:, range(1, are_neighbors.shape[1]),range(0, are_neighbors.shape[1] - 1)] = -1
        features = torch.cat((square_dists[...,None],aacs[:,:,None,:].expand(-1,-1,coordinates.shape[1],-1),aacs[:,None,:,:].expand(-1,coordinates.shape[1],-1,-1),are_neighbors[...,None]),-1)

        features.masked_fill_(mask[:,None,:,None], 0.0)
        features.masked_fill_(mask[:, :, None, None], 0.0)
        self_mask = torch.diag(mask.new_ones(features.size(1)).int()).bool()
        features.masked_fill_(self_mask[None,:,:,None], 0.0)

        pairwise_forces = self.layers(features) * dist_diffs
        # pairwise_forces = self.layers(features.reshape(coordinates.size(0),coordinates.size(1)*coordinates.size(1),-1))
        # pairwise_forces = pairwise_forces.reshape(coordinates.size(0),coordinates.size(1),coordinates.size(1),-1)
        # pairwise_forces = pairwise_forces*dist_diffs
        return torch.sum(pairwise_forces,2)

if __name__=='__main__':
    coordinates = torch.randn(3, 30, 3)
    aacs = torch.randn(3, 30, 4)

    mask = torch.zeros((3,30),dtype=torch.int8)
    mask[0,[26,27,28,29]] = 1
    mask[1, [29]] = 1

    mlp = MlpInteraction(4,[32,32],True)
    x = mlp(coordinates,aacs,mask==1)
    print(x)