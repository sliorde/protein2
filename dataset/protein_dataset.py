import os
from typing import Optional,Union
import pickle
import tarfile

import numpy as np
import torch
from torchtext.data import Example, Dataset, Field, BucketIterator
from torchtext.utils import download_from_url

# dictionary of train,validation and test dataset files
data_files = {'train':['./data/training_30.pickle', './data/training_50.pickle', './data/training_70.pickle', './data/training_90.pickle','./data/training_95.pickle', './data/training_100.pickle'],
              'validation':['./data/validation.pickle'],
              'testing':['./data/testing.pickle']}

def download_and_preprocess():
    path = './data/casp11.tar.gz'
    download_from_url('https://sharehost.hms.harvard.edu/sysbio/alquraishi/proteinnet/human_readable/casp11.tar.gz',path)
    tar = tarfile.open(path, "r:gz")
    tar.extractall()
    tar.close()
    os.remove(path)

    filenames = ['./data/training_30', './data/training_50', './data/training_70', './data/training_90',
                   './data/training_95', './data/training_100'] + ['./data/validation'] + ['./data/testing']

    parse_text_to_protein(filenames)

def parse_text_to_protein(filenames):
    cnt = 0
    for filename in filenames:

        id = None
        primary = None
        tertiary = None
        mask = None

        all_proteins = []
        with open(filename, mode='r') as f:
            line = f.readline()
            while True:
                if '[ID]' in line:
                    if id is not None:
                        raise RuntimeError('id')
                    else:
                        line = f.readline()
                        id = line[:-1]
                elif '[PRIMARY]' in line:
                    if primary is not None:
                        raise RuntimeError('primary')
                    else:
                        line = f.readline()
                        convert_to_num = {k: v for k, v in zip('ACDEFGHIKLMNPQRSTVWY', range(1, 21))}
                        primary = np.array(list(map(lambda x: convert_to_num[x], line[:-1])), dtype=np.int8)
                elif '[TERTIARY]' in line:
                    if tertiary is not None:
                        raise RuntimeError('tertiary')
                    else:
                        line = f.readline()
                        tertiary1 = np.fromstring(line[:-1], sep='\t')
                        line = f.readline()
                        tertiary2 = np.fromstring(line[:-1], sep='\t')
                        line = f.readline()
                        tertiary3 = np.fromstring(line[:-1], sep='\t')
                        tertiary = np.stack((tertiary1, tertiary2, tertiary3), 1)
                        tertiary = tertiary[1::3, :]  # take only Ca atom, discard remaining two atoms for each AAC
                elif '[MASK]' in line:
                    if mask is not None:
                        raise RuntimeError('mask')
                    else:
                        line = f.readline()
                        convert_to_num = {'-': False, '+': True}
                        mask = np.array(list(map(lambda x: convert_to_num[x], line[:-1])))

                        # TODO: there may be cases where mask is zero\one lengthed, in which case we should use `known_if_aac_measured`

                if (id is not None) and (primary is not None) and (tertiary is not None) and (mask is not None):
                    all_proteins.append({'id': id, 'sequence': primary, 'coordinates': tertiary, 'aac_measured': mask})

                    cnt += 1
                    if (cnt % 100 == 0):
                        print('downloading from {:s}: (proten #{:d})'.format(filename, cnt))

                    id = None
                    primary = None
                    tertiary = None
                    mask = None

                line = f.readline()
                if line == '':
                    break

        os.remove(filename)

        with open(filename + '.pickle', 'wb') as f:
            pickle.dump(all_proteins, f)

def get_protein_dataset_and_dataiterator(files,max_size:Optional[int]=None,num_dimensions:int=3,coordinate_padding:float=0.0,also_iterator=True,batch_size:int=100000, protein_length_exponent_for_batch_size:int=2,shuffle:bool=True,device:Union[str,torch.device]='cpu'):
    """

    get a PyTorch Dataset, and possibly also an Iterator, for proteins.
    Each example in the dataset will have the fields:
        `sequence` - a sequence of numbers representing the amino acid type
        `coordinates` - a sequence of 3d vectors representing the amino acid location in the folded protein
        `aac_measured` - a boolean sequence representing whether this amino acid's location was measured.
    If an iterator is also returned, then the batches will be padded if not all proteins are the same length.

    Args:
        files: list of files for dataset
        max_size: use proteins up to this size
        num_dimensions:
        coordinate_padding:
        also_iterator:
        batch_size:
        protein_length_exponent_for_batch_size:
        shuffle:
        device:

    Returns:

    """
    if max_size is None:
        max_size = 2**31

    def tokenizer(x):
        return x

    fields = [
        ('sequence', Field(sequential=True, use_vocab=False, dtype=torch.int64, tokenize=tokenizer,
                           include_lengths=False,batch_first=True, is_target=False, pad_token=0)),
        ('coordinates', Field(sequential=True, use_vocab=False, dtype=torch.float32, tokenize=tokenizer,
                             include_lengths=False, batch_first=True, is_target=True, pad_token=coordinate_padding*np.ones(num_dimensions))),
        ('aac_measured', Field(sequential=True, use_vocab=False, dtype=torch.int32, tokenize=tokenizer,
                              include_lengths=False, batch_first=True, is_target=True, pad_token=0))]

    examples = []
    for file in files:
        with open(file, 'rb') as f:
            examples += [Example.fromdict(ex, {k:(k,v) for k,v in fields}) for ex in pickle.load(f)]

    ds = Dataset(examples,fields,lambda ex: len(ex.sequence)<=max_size)

    if also_iterator:
        # we create a bucker iterator with a batch_size_fn that makes the batch size depend on the length of the proteins to the power of `protein_length_exponent_for_batch_size`

        def batch_size_fn(new, count, sofar):
            new_bs = sofar + len(new.sequence) ** protein_length_exponent_for_batch_size
            if count==1:
                return min(new_bs,batch_size)
            else:
                return  new_bs

        dl = BucketIterator(ds, batch_size, sort_key=lambda ex: len(ex.sequence), batch_size_fn=batch_size_fn, shuffle=shuffle,device=device)
        return ds,dl
    else:
        return ds

if __name__=='__main__':

    ds,dl = get_protein_dataset_and_dataiterator(data_files['train'],max_size=None,batch_size=100000,protein_length_exponent_for_batch_size=2)

    dl_iter = iter(dl)
    batch = next(dl_iter)
    print(batch.sequence)
    print(batch.coordinates)
    print(batch.aac_measured)