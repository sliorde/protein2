import numpy as np
import pickle

filenames = ['testing','training_100','training_30','training_50','training_70','training_90','training_95','validation']

cnt = 0
for filename in filenames:

    print(filename)

    id = None
    primary = None
    tertiary = None
    mask = None

    all_proteins = []
    with open(filename,mode='r') as f:
        line = f.readline()
        while True:
            if '[ID]' in line:
                if id is not None:
                    raise RuntimeError('id')
                else:
                    line = f.readline()
                    id = line[:-1]
            elif '[PRIMARY]' in line:
                if primary is not None:
                    raise RuntimeError('primary')
                else:
                    line = f.readline()
                    convert_to_num = {k:v for k,v in zip('ACDEFGHIKLMNPQRSTVWY',range(1,21))}
                    primary = np.array(list(map(lambda x: convert_to_num[x],line[:-1])),dtype=np.int8)
            elif '[TERTIARY]' in line:
                if tertiary is not None:
                    raise RuntimeError('tertiary')
                else:
                    line = f.readline()
                    tertiary1 = np.fromstring(line[:-1], sep='\t')
                    line = f.readline()
                    tertiary2 = np.fromstring(line[:-1], sep='\t')
                    line = f.readline()
                    tertiary3 = np.fromstring(line[:-1], sep='\t')
                    tertiary = np.stack((tertiary1,tertiary2,tertiary3),1)
                    tertiary = tertiary[1::3,:] # take only Ca atom, discard remaining two atoms for each AAC
            elif '[MASK]' in line:
                if mask is not None:
                    raise RuntimeError('mask')
                else:
                    line = f.readline()
                    convert_to_num = {'-':False,'+':True}
                    mask = np.array(list(map(lambda x: convert_to_num[x],line[:-1])))

                    # TODO: there may be cases where mask is zero\one lengthed, in which case we should use `known_if_aac_measured`

            if (id is not None) and (primary is not None) and (tertiary is not None) and (mask is not None):
                all_proteins.append({'id':id,'sequence':primary,'coordinates':tertiary,'aac_measured':mask})

                cnt +=1
                if (cnt % 100 == 0):
                    print('{:s}:{:d}'.format(filename,cnt))

                id = None
                primary = None
                tertiary = None
                mask = None

            line = f.readline()
            if line=='':
                break

    with open(filename+'.pickle','wb') as f:
        pickle.dump(all_proteins,f)