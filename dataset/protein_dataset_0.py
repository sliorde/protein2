import pickle
from typing import Optional

import numpy as np
import torch
from torch.utils.data import DataLoader, Dataset
from torch.nn.utils.rnn import pad_sequence
from torchtext.data import BucketIterator
from torchtext.data import Field, Example, Dataset, Field, Batch


from arguments import Args


class ProteinDataset(Dataset):

    train_files = ['training_30.pickle','training_50.pickle','training_70.pickle','training_90.pickle','training_95.pickle']
    validation_files = ['validation.pickle']
    testing_files = ['testing.pickle']

    def __init__(self,mode:str,max_size:Optional[int]=None):
        assert mode.upper() in ['TRAIN','EVAL']

        if mode.upper() == 'TRAIN':
            files = self.train_files
        else:
            files = self.validation_files

        if max_size is None:
            max_size = float('Inf')

        self.examples = []
        for file in files:
            try:
                with open(file,'rb') as f:
                    examples = pickle.load(f)
                    for ex in examples:
                        ex['sequence'] = torch.from_numpy(ex['sequence'].astype(np.int32))
                        ex['coordinates'] = torch.from_numpy(ex['coordinates'])
                        ex['aac_measured'] = torch.from_numpy(ex['aac_measured'].astype(np.int32))
                    self.examples += examples
            except FileNotFoundError:
                continue

    def __getitem__(self, i):
        return self.examples[i]

    def __len__(self):
        return len(self.examples)


def collate_with_padding(examples):
    max_len = 0
    for ex in examples:
        max_len = max(max_len,len(ex['sequence']))
    ids = [ex['id'] for ex in examples]
    sequences = pad_sequence([ex['sequence'] for ex in examples],batch_first=True,padding_value=0)
    coordinates = pad_sequence([ex['coordinates'] for ex in examples], batch_first=True, padding_value=0.0)
    aac_measured = pad_sequence([ex['aac_measured'] for ex in examples], batch_first=True, padding_value=-1)

    return ids,sequences,coordinates,aac_measured

dataset = ProteinDataset('EVAL',80)

dl = DataLoader(dataset,batch_size=10,collate_fn=collate_with_padding)

batch = next(iter(dl))

aaa=1
