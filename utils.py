import sys
import logging
from pathlib import Path
import inspect
import datetime
import os
import git
import random
import pickle
import numpy as np
import torch

def startup(main_file, main_name, log_to_file=True, log_to_console=True):
    output_dir = get_output_dir(os.path.splitext(os.path.basename(main_file))[0])
    output_file = setup_logging(output_dir, log_to_file, log_to_console)
    repo_path = log_gIt()
    log_modules(repo_path, main_name)
    logger = logging.getLogger('protein_logger')
    logger.info('sys.argv:  {}'.format(sys.argv))
    logger.info('output dir: {:s}'.format(output_dir))
    if output_file is not None:
        logger.info('output file: {:s}'.format(output_file))
    return output_dir

def get_output_dir(name):
    t = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S-%f')
    output_dir = os.path.join('output', name, t)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    return output_dir

def setup_logging(output_dir, log_to_file=True, log_to_console=True):
    log_format = logging.Formatter("%(asctime)s : %(message)s")
    logger = logging.getLogger('protein_logger')
    for handle in logger.handlers:
        logger.removeHandler(handle)
    if log_to_file:
        output_file = os.path.join(output_dir, 'output.log')
        file_handler = logging.FileHandler(output_file)
        file_handler.setFormatter(log_format)
        logger.addHandler(file_handler)
    else:
        output_file = None
    if log_to_console:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_format)
        logger.addHandler(console_handler)
    logger.setLevel(logging.INFO)
    return output_file

def log_gIt():
    logger = logging.getLogger('protein_logger')
    try:
        repo = git.Repo(search_parent_directories=True)
    except git.InvalidGitRepositoryError:
        logger.info('not part of a git repository.')
        return os.path.dirname(os.path.abspath(__file__))
    sha = repo.head.object.hexsha
    branch = repo.active_branch.name
    logger.info('git directory: {}'.format(repo.working_dir))
    logger.info('git branch: {}'.format(branch))
    logger.info('git hash of last commit: {}'.format(sha))
    modified = [item.a_path for item in repo.index.diff(None)]
    logger.info('git modified files: {}'.format(modified))
    return repo.working_dir

def log_modules(repo_path:str, main_name:str):

    # 1) log sources of all imported project modules
    repo_path = Path(repo_path)
    main_module = sys.modules[main_name]
    logger = logging.getLogger('protein_logger')
    if not inspect.isbuiltin(main_module):
        try:
            file = Path(inspect.getfile(main_module))
        except TypeError:
            file = None
        if file is not None:
            try:
                logger.info('source of main file ({:s})  :       {:s}'.format(str(file.relative_to(repo_path)),
                                                                      str(inspect.getsourcelines(main_module))))
            except ValueError:
                logger.info('source of main file  :       {:s}'.format(str(inspect.getsourcelines(main_module))))
    for name, module in sys.modules.items():
        if (module is not main_module) and (not inspect.isbuiltin(module)):
            try:
                file = Path(inspect.getfile(module))
            except TypeError:
                continue
            if repo_path in file.parents:
                logger.info('source of {:s} :       {:s}'.format(str(file.relative_to(repo_path)),str(inspect.getsourcelines(sys.modules[name]))))

    # 2) log versions of all imported moduls
    modules = []
    options = ['__version__', 'version', '__VERSION__', 'VERSION', '__ver__', 'ver', '__VER__', 'VER']
    for name, module in sys.modules.items():
        ver = None
        for option in options:
            try:
                ver = eval('module.'+option)
                if ver is not None:
                    break
            except:
                pass
        if ver is not None:
            modules.append(name+':'+str(ver) + ' ')
    logger = logging.getLogger('protein_logger')
    if len(modules) > 0:
        logger.info('module versions: {}'.format(modules))
    logger.info('Python version: {}'.format(sys.version))

def log_args(**kwargs):
    logger = logging.getLogger('protein_logger')
    for k,v in kwargs.items():
        logger.info('{:s} :  {:s}'.format(k,str(v)))

def set_random_seeds(seed):
    np.random.seed(seed)

    new_seed = np.random.randint(0,10000000)
    random.seed(new_seed)

    new_seed = np.random.randint(0, 10000000)
    torch.manual_seed(new_seed)

    new_seed = np.random.randint(0, 10000000)
    torch.cuda.manual_seed_all(new_seed)

    new_seed = np.random.randint(0, 10000000)
    np.random.seed(new_seed)

def save_args_to_pickle(filename, args):
    with open(filename,'wb') as f:
        pickle.dump(args,f)
    logger = logging.getLogger('protein_logger')
    logger.info('saved args to file: {:s}'.format(filename))

def load_args_from_pickle(filename):
    with open(filename,'rb') as f:
        args = pickle.load(f)
    return args

def update_args_from_sys_argv(args, sys_args):
    for sys_arg in sys_args[1:]:
        arg, val = sys_arg.split('=')
        if hasattr(args, arg):
            try:
                val = int(val)
            except ValueError:
                try:
                    val = float(val)
                except:
                    pass
            setattr(args, arg, val)
        else:
            raise ValueError('wrong system arg')
