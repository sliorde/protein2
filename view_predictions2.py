import os
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import animation
import torch

from utils import set_random_seeds, load_args_from_pickle
from math_utils import align
from network import Network
from network3 import Network3
from dataset.protein_dataset import get_protein_dataset_and_dataiterator
from protein import Protein


RESTORE_CHECKPOINT = r'output/main2/2019-08-07-06-05-33-282715/checkpoint.ckpt'

args = load_args_from_pickle(os.path.join(os.path.dirname(RESTORE_CHECKPOINT), 'arguments.pickle'))

args.max_protein_length = 50

set_random_seeds(92821)

device = 'cuda' if torch.cuda.is_available() else 'cpu'


net = Network3(args)
net.to(device)

net.restore_checkpoint(RESTORE_CHECKPOINT,also_optimizer=False,device=device)

_,validation_iterator = get_protein_dataset_and_dataiterator(args.validation_set_files, args.max_protein_length,
                                                               coordinate_padding=args.aas_coordinate_padding,shuffle=True,batch_size=args.effective_batch_size)
validation_iterator = iter(validation_iterator)

def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

repetitions = 1
how_many_to_show = 100
i = 0
j = 0
l = 0

with torch.no_grad():
    while i < how_many_to_show:
        if (j==l):
            try:
                batch = next(validation_iterator)
            except StopIteration:
                break
            l = batch.sequence.shape[0]
            length = torch.sum(batch.sequence > 0, 1)
            j = 0
        c = net.predict_dynamics(batch.sequence[[j]],length[[j]],rep=repetitions)#,p.coordinates[[j]])
        c_true = [None for _ in c]
        for k in range(len(c)):
            c[k] = c[k][0][:length[j]]
            c_true[k],c[k] = align(batch.coordinates[[j]][0][:length[j]]*args.coordinates_factor,c[k],batch.aac_measured[[j]][0][:length[j]])
            c_true[k] = c_true[k].numpy()
            c[k] = c[k].numpy()


        p2 = Protein("___",batch.sequence.numpy(),length.numpy(),batch.coordinates.numpy()*args.coordinates_factor,batch.aac_measured.numpy(),False)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        inds = p2.aac_measured[[j]][0,:length[j]].astype(np.bool)
        line1, = plt.plot(c_true[0][inds, 0], c_true[0][inds, 1], c_true[0][inds, 2], linewidth=1,color=(66/255,151/255,236/255),alpha=None,animated=True,marker='o',markeredgecolor='k',markeredgewidth=0.3,markerfacecolor=(30/255,51/255,240/255),markersize=5,)
        line2, = plt.plot(c[0][inds, 0], c[0][inds, 1], c[0][inds, 2], linewidth=1, color=(34/255,177/255,76/255),alpha=None,animated=True,marker='o',markeredgecolor='k',markeredgewidth=0.3,markerfacecolor=(19/255,102/255,44/255),markersize=5,)
        lines = [plt.plot((c[0][i, 0],c_true[0][i, 0]), (c[0][i, 1],c_true[0][i, 1]), (c[0][i, 2],c_true[0][i, 2]), linewidth=1, color=(0.4,0.4,0.4),linestyle=':',alpha=None,animated=True)[0] for i in np.nonzero(inds)[0]]
        title = ax.text2D(0.2, 0.95, '___', transform=ax.transAxes)
        ax.set_xlim(np.min(c_true[0][inds, 0]),np.max(c_true[0][inds, 0]))
        ax.set_ylim(np.min(c_true[0][inds, 1]), np.max(c_true[0][inds, 1]))
        ax.set_zlim(np.min(c_true[0][inds, 2]), np.max(c_true[0][inds, 2]))

        def init():
            line1.set_data(c_true[0][inds, 0], c_true[0][inds, 1])
            line1.set_3d_properties(c_true[0][inds, 2])
            line2.set_data(c[0][inds, 0], c[0][inds, 1])
            line2.set_3d_properties(c[0][inds, 2])
            for i in np.nonzero(inds)[0]:
                lines[i].set_data((c[0][i, 0],c_true[0][i, 0]), (c[0][i, 1],c_true[0][i, 1]))
                lines[i].set_3d_properties((c[0][i, 2],c_true[0][i, 2]))
            title.set_text('id={:s}.  length={:d}.'.format('____',p2.length[j]))
            out = (line1,line2) + tuple(lines) + (title,)
            return out

        def animate(k):
            line1.set_data(c_true[k][inds, 0], c_true[k][inds, 1])
            line1.set_3d_properties(c_true[k][inds, 2])
            line2.set_data(c[k][:, 0], c[k][:, 1])
            line2.set_3d_properties(c[k][:, 2])
            for i in np.nonzero(inds)[0]:
                lines[i].set_data((c[k][i, 0],c_true[k][i, 0]), (c[k][i, 1],c_true[k][i, 1]))
                lines[i].set_3d_properties((c[k][i, 2],c_true[k][i, 2]))
            title.set_text('id={:s}.  length={:d}.  step={:d}/{:d}'.format('____', p2.length[j], k + 1, len(c_true)))
            out = (line1, line2) + tuple(lines) + (title,)
            return out


        anim = animation.FuncAnimation(fig, animate, init_func=init,
                                       frames=len(c), interval=100, repeat=True, repeat_delay=1000, blit=True)
        axisEqual3D(ax)
        ax.set_proj_type('ortho')

        plt.show()

        i +=1
        j +=1