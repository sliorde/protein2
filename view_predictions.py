import os
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import animation
import torch

from utils import set_random_seeds, load_args_from_pickle
from math_utils import align
from network import Network
from network2 import Network2
from data_fetcher import DataFetcher
from protein import Protein


RESTORE_CHECKPOINT = r'output\main\2019-02-23-21-32-05-801129\checkpoint.ckpt'

args = load_args_from_pickle(os.path.join(os.path.dirname(RESTORE_CHECKPOINT), 'arguments.pickle'))

args.max_protein_length = 22
args.training_set_path = args.validation_set_path
args.preprocessed_training_set_path = args.preprocessed_validation_set_path
args.training_lengths = args.validation_lengths
args.training_accum_examples = args.validation_accum_examples

set_random_seeds(92821)

device = 'cuda' if torch.cuda.is_available() else 'cpu'


net = Network2(args)
net.to(device)

net.restore_checkpoint(RESTORE_CHECKPOINT,also_optimizer=False,device=device)

fetcher = DataFetcher('EVAL',args,use_preprocessed=False)

def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

repetitions = 3
how_many_to_show = 1
i = 0
j = 0

with torch.no_grad():
    while i < how_many_to_show:
        if (i==0) or (j==len(p.id)):
            p = fetcher.get_next(device)
            j = 0
        c = net.predict_dynamics(p.sequence[[j]],p.length[[j]],repetitions)#,p.coordinates[[j]])
        c_true = [None for _ in c]
        for k in range(len(c)):
            c_true[k],c[k] = align(p.coordinates[[j]][0],c[k][0],p.aac_measured[[j]][0])
            c_true[k] = c_true[k].numpy()
            c[k] = c[k].numpy()

        p2 = Protein(p.id,p.sequence.numpy(),p.length.numpy(),p.coordinates.numpy(),p.aac_measured.numpy(),p.known_if_aac_measured)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        inds = p2.aac_measured[[j]][0,:].astype(np.bool)
        line1, = plt.plot(c_true[0][inds, 0], c_true[0][inds, 1], c_true[0][inds, 2], c='b')
        line2, = plt.plot(c[0][inds, 0], c[0][inds, 1], c[0][inds, 2], c='g')
        title = ax.text2D(0.2, 0.95, p2.id[[j]][0][0], transform=ax.transAxes)
        ax.set_xlim(np.min(c_true[0][inds, 0]),np.max(c_true[0][inds, 0]))
        ax.set_ylim(np.min(c_true[0][inds, 1]), np.max(c_true[0][inds, 1]))
        ax.set_zlim(np.min(c_true[0][inds, 2]), np.max(c_true[0][inds, 2]))

        def init():
            line1.set_data(c_true[0][inds, 0], c_true[0][inds, 1])
            line1.set_3d_properties(c_true[0][inds, 2])
            line2.set_data(c[0][inds, 0], c[0][inds, 1])
            line2.set_3d_properties(c[0][inds, 2])
            title.set_text('id={:s}.  length={:d}.'.format(str(p2.id[[j]][0][0]),p2.length[[j]][0][0]))
            return line1, line2, title

        def animate(k):
            line1.set_data(c_true[k][inds, 0], c_true[k][inds, 1])
            line1.set_3d_properties(c_true[k][inds, 2])
            line2.set_data(c[k][:, 0], c[k][:, 1])
            line2.set_3d_properties(c[k][:, 2])
            title.set_text('id={:s}.  length={:d}.  step={:d}/{:d}'.format(str(p2.id[[j]][0][0]), p2.length[[j]][0][0], k + 1, len(c_true)))
            return line1,line2,title


        anim = animation.FuncAnimation(fig, animate, init_func=init,
                                       frames=len(c), interval=100, repeat=True, repeat_delay=1000, blit=True)
        axisEqual3D(ax)
        ax.set_proj_type('ortho')

        plt.show()

        i +=1
        j +=1