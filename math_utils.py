from math import pi as PI
from typing import Optional

import torch


def random_rotation_matrix(batch_size, num_dimensions=3,device='cpu'):
    """
    create `batch_size` random rotation matrices.
    Args:
        batch_size:
        num_dimensions:
        device:

    Returns:

    """
    if num_dimensions != 3:
        raise ValueError('Dimension must be 3 for rotation matrices')
    theta = torch.acos(2*torch.rand(size=(batch_size,),device=device)-1.0)
    phi = 2.0*PI*torch.rand((batch_size,),device=device)
    u1 = torch.sin(theta)*torch.cos(phi)
    u2 = torch.sin(theta)*torch.sin(phi)
    u3 = torch.cos(theta)
    angle = 2.0*PI*torch.rand((batch_size,),device=device)
    cos_angle = torch.cos(angle)
    one_minus_cos_angle = 1-cos_angle
    sin_angle = torch.sin(angle)

    r12 = u1 * u2 * one_minus_cos_angle - u3*sin_angle
    r13 = u1 * u3 * one_minus_cos_angle + u2*sin_angle
    r21 = u2 * u1 * one_minus_cos_angle + u3*sin_angle
    r23 = u2 * u3 * one_minus_cos_angle - u1 * sin_angle
    r31 = u3 * u1 * one_minus_cos_angle - u2 * sin_angle
    r32 = u3 * u2 * one_minus_cos_angle + u1 * sin_angle
    r11 = cos_angle + u1*u1 * one_minus_cos_angle
    r33 = cos_angle + u3*u3 * one_minus_cos_angle
    r22 = cos_angle + u2*u2 * one_minus_cos_angle

    rotation = torch.reshape(torch.stack([r11,r12,r13,r21,r22,r23,r31,r32,r33],1),[batch_size,3,3])
    return rotation

def align(coordinates1:torch.Tensor, coordinates2:torch.Tensor, mask:Optional[torch.Tensor]=None, returned_masked:bool=False):
    """
    use SVD to translate and rotate the sequence of 3D coordinates  `coordinates2` such that they are best aligned with `coordinates1` in terms of L2 distance
    """
    try:
        device = coordinates1.device
    except RuntimeError:
        device = 'cpu'
    if mask is not None:
        mask = mask.view(-1, 1).repeat(1, 3).bool()
        c1 = torch.masked_select(coordinates1, mask).view(-1, coordinates1.shape[-1])
        c2 = torch.masked_select(coordinates2, mask).view(-1, coordinates2.shape[-1])
    else:
        c1 = coordinates1
        c2 = coordinates2
    avg_c1 = torch.mean(c1, 0)
    c1 = c1 - avg_c1
    avg_c2 = torch.mean(c2, 0)
    c2 = c2 - avg_c2
    u,_,v = torch.svd(torch.matmul(torch.t(c2),c1,))
    s = torch.diag(torch.cat([torch.ones(c1.shape[1]-1,device=device),torch.unsqueeze(torch.sign(torch.det(u)*torch.det(v)),0)],0))
    rotation_matrix = torch.matmul(torch.matmul(u,s),torch.t(v))
    if returned_masked:
        c1 = c1 + avg_c1
        c2 = torch.matmul(c2,rotation_matrix) + avg_c1
        return c1,c2
    else:
        coordinates2 = torch.matmul(coordinates2-avg_c2,rotation_matrix) + avg_c1
        return coordinates1,coordinates2

def align_batch(coordinates1:torch.Tensor, coordinates2:torch.Tensor, mask:Optional[torch.Tensor]=None, returned_masked:bool=False):
    """
    TODO: this function is not done!!!
    use batched SVD to do the same thing as the function `align`
    Args:
        coordinates1:
        coordinates2:
        mask:
        returned_masked:

    Returns:

    """
    device = coordinates1.device
    if 'cuda' not in device:
        raise TypeError('the function `align_batch` can only be run on GPU')

    if mask is not None:
        l = torch.sum(mask, 1)
        negative_mask = 1-mask

        c1 = coordinates1.masked_fill(negative_mask[:,:,None],0.0)
        mean = torch.sum(c1,1,keepdim=True)/(l.float()[:,None,None])
        mean.repeat(1,c1.size(1),1)
        c1[negative_mask[:,:,None].repeat(1,1,3)] = mean.repeat(1,c1.size(1),1)[negative_mask[:,:,None].repeat(1,1,3)]

        c2 = coordinates1.masked_fill(negative_mask[:, :, None], 0.0)
        mean = torch.sum(c2, 1, keepdim=True) / (l.float()[:, None, None])
        mean.repeat(1, c2.size(1), 1)
        c2[negative_mask[:, :, None].repeat(1, 1, 3)] = mean.repeat(1, c2.size(1), 1)[
            negative_mask[:, :, None].repeat(1, 1, 3)]
    else:
        c1 = coordinates1
        c2 = coordinates2
    avg_c1 = torch.mean(c1, 1,keepdim=True)
    c1 = c1 - avg_c1
    avg_c2 = torch.mean(c2, 1,keepdim=True)
    c2 = c2 - avg_c2
    u,_,v = batch_svd(torch.matmul(c2.transpose(1,2),c1,))

    # TODO: stopped before this line... continue implementation of batched svd
    s = torch.diag(torch.cat([torch.ones(c1.shape[1]-1,device=device),torch.unsqueeze(torch.sign(torch.det(u)*torch.det(v)),0)],0))
    rotation_matrix = torch.matmul(torch.matmul(u,s),torch.t(v))
    if returned_masked:
        c1 = c1 + avg_c1
        c2 = torch.matmul(c2,rotation_matrix) + avg_c1
        return c1,c2
    else:
        coordinates2 = torch.matmul(coordinates2-avg_c2,rotation_matrix) + avg_c1
        return coordinates1,coordinates2

def mean_squared_distances(predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, mask:Optional[torch.Tensor]=None, with_batch:bool=True):
    """
    calculate L2 loss between `predicted_coordinates` and `true_coordinates`, where the loss makes sure to rotate the coordinates with the optimal rotation that aligns between the two sequences of coordinates.
    Args:
        predicted_coordinates:
        true_coordinates:
        mask:
        with_batch:

    Returns:

    """
    if not with_batch:
        c1, c2 = align(predicted_coordinates, true_coordinates, mask, returned_masked=True)
        c2 = c2.detach()
        msd = torch.mean((c1-c2)**2)
    else:
        msd = []
        if mask is not None:
            for c1,c2,m in zip(predicted_coordinates,true_coordinates,mask):
                msd.append(mean_squared_distances(c1, c2, m, False))
        else:
            for c1,c2,m in zip(predicted_coordinates,true_coordinates):
                msd.append(mean_squared_distances(c1, c2, None, False))
        msd = torch.stack(msd)
    return msd