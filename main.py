import os
import sys

import cProfile

import torch

from arguments import args
from data_fetcher import DataFetcher
from network import Network
from network2 import Network2
from utils import startup, log_args, set_random_seeds, save_args_to_pickle, load_args_from_pickle,update_args_from_sys_argv

output_dir = startup(__file__, __name__)

PROFILE = False
PROFILER_EPOCHS = 10

RESTORE_CHECKPOINT = 'output/main/2019-02-23-21-32-05-801129/checkpoint.ckpt'
ALSO_RESTORE_ARGUMENTS = True
ALSO_RESTORE_OPTIMIZER = False
DO_VALIDATION = True

WHICH = 'NETWORK2'

if (RESTORE_CHECKPOINT is not None) and ALSO_RESTORE_ARGUMENTS:
    args = load_args_from_pickle(os.path.join(os.path.dirname(RESTORE_CHECKPOINT),'arguments.pickle'))

args.seed = 213145

args.max_protein_length = 22
# args.training_set_path = args.validation_set_path
# args.preprocessed_training_set_path = args.preprocessed_validation_set_path
# args.training_lengths = args.validation_lengths
# args.training_accum_examples = args.validation_accum_examples

args.batch_size =  32

args.network1_interaction_hidden_layer_sizes = [128, 128, 128]
args.network1_neighbor_hidden_layer_sizes = [32, 32]

args.aac_embedding_size = 8

args.network2_num_layers = 6
args.network2_num_heads = 32
args.network2_model_dim = 100
args.network2_query_dim = 16
args.network2_inner_dim = 400
args.network2_dropout = 0.1
args.network2_dropatt = 0.1
args.network2_pre_lnorm = False
args.network2_init_type = 'UNIFORM'
args.network2_initialization_scale_proj = 1e-1

args.num_repetitions = 12

args.add_noise_initial_training = True # True
args.add_noise_in_training = False
args.noise_in_training_interval = 6
args.coordinate_perturbation_factor = 0.06

args.use_final_states = False
args.weight_final_states = 0.1
args.steps_final_states = 1

args.use_inermediate_gradients = True
args.inermediate_gradients_interval = 1
args.inermediate_gradients_exponent = 2

args.neighboor_dist_coeff = None

args.optimizer = 'ADAM'
args.learning_rate = 2e-5
args.initialization_scale = 1e-3

args.train_eval_frequency = 5
args.test_eval_frequency = 1  #1

update_args_from_sys_argv(args,sys.argv)

log_args(WHICH=WHICH, args=args, RESTORE_CHECKPOINT=RESTORE_CHECKPOINT, ALSO_RESTORE_ARGUMENTS=ALSO_RESTORE_ARGUMENTS, ALSO_RESTORE_OPTIMIZER=ALSO_RESTORE_OPTIMIZER,DO_VALIDATION=DO_VALIDATION)

save_args_to_pickle(os.path.join(output_dir, 'arguments.pickle'),args)

set_random_seeds(args.seed)

train_fetcher = DataFetcher('TRAIN',args,use_preprocessed=False)
if DO_VALIDATION:
    validation_fetcher = DataFetcher('EVAL', args, use_preprocessed=False)
else:
    validation_fetcher = None

if WHICH.upper()=='NETWORK1':
    net = Network(args)
elif WHICH.upper()=='NETWORK2':
    net = Network2(args)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

if RESTORE_CHECKPOINT is not None:
    net.restore_checkpoint(RESTORE_CHECKPOINT,ALSO_RESTORE_OPTIMIZER,device)

net.to(device)

if PROFILE:
    cProfile.run(
        'net.train_model(train_fetcher, validation_data_fetcher=None, dir=None, device=device,max_epochs=PROFILER_EPOCHS)',
        'profiler_stats')
else:
    net.train_model(train_fetcher, validation_fetcher, output_dir, device)