import os
import logging
from typing import Optional, List
import traceback
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import functional as F
from torchtext.data import BucketIterator

from arguments2 import Args
from math_utils import random_rotation_matrix, mean_squared_distances
from mlp import MlpInteraction
from transformer import Transformer

class AdaptiveEmbedding(nn.Module):
    def __init__(self, sz_vocab, embed_dim, embed_out_dim):
        super(AdaptiveEmbedding, self).__init__()

        self.embed_scale = embed_out_dim ** 0.5

        self.embed_layer = nn.Embedding(sz_vocab, embed_dim,padding_idx=0)

        self.with_proj = False
        if embed_out_dim != embed_dim:
            self.with_proj = True
            self.embed_proj = nn.Parameter(torch.Tensor(embed_out_dim, embed_dim))

    def forward(self, x):
        embed = self.embed_layer(x)
        if self.with_proj:
            embed  = F.linear(embed, self.embed_proj)
        embed.mul_(self.embed_scale)
        return embed

class Network3(nn.Module):
    def __init__(self,args:Args):
        super(Network3,self).__init__()

        self.args = args

        self.add_embedding_ops()

        self.add_interaction_ops()

        self.add_convolution_ops()

        self.steps = torch.tensor(0,dtype=torch.int32)
        self.register_buffer('steps_',self.steps)
        self.examples = torch.tensor(0,dtype=torch.int32)
        self.register_buffer('examples_',self.examples)
        self.learning_rate = torch.tensor(0.0)
        self.register_buffer('learning_rate_',self.learning_rate)

        self.optimizer = None

        if self.args.use_transformer:
            initializer = lambda m: initialize_weights_transformer(m, self.args.init_type, args.initialization_scale, self.args.initialization_scale_proj)
            self.apply(initializer)
            if self.interaction.resize is None:
                nn.init.zeros_(self.interaction.self.layers[-1].ff.layer_norm.weight)
                nn.init.zeros_(self.interaction.self.layers[-1].ff.layer_norm.bias)
            else:
                nn.init.zeros_(self.interaction.resize.weight)
                nn.init.zeros_(self.interaction.resize.bias)
        else:
            self.interaction.initialize_weights(args.initialization_scale)


        
        self.fward = lambda sequence, length, initial_coordinates=None, repetitions=None, true_coordinates=None, aac_measured=None: self(sequence, length, initial_coordinates, repetitions, true_coordinates, aac_measured)


        logger = logging.getLogger('protein_logger')
        logger.info('initialized network. number of parameters: {:d}'.format(self.number_of_parameters()))

    def forward(self, sequence:torch.Tensor,length:torch.Tensor,initial_coordinates:Optional[torch.Tensor]=None,repetitions:Optional[int]=None,true_coordinates:Optional[torch.tensor]=None,aac_measured:Optional[torch.tensor]=None):

        if repetitions is None:
            repetitions = self.args.num_repetitions

        sequence_embedded = self.embed(sequence)

        inds_out = torch.eq(sequence,0)

        if initial_coordinates is None:
            initial_coordinates = self.get_initial_coordinates(sequence, length, inds_out)

        if self.training and self.args.add_noise_initial_training:
            initial_coordinates = self.pertrub_coordinates(initial_coordinates)
            initial_coordinates = torch.einsum('bij,bkj->bki', random_rotation_matrix(sequence.shape[0], self.args.num_dimensions,device=initial_coordinates.device), initial_coordinates)
            initial_coordinates.masked_fill_(inds_out[:, :, None], self.args.aas_coordinate_padding)

        coordinates = initial_coordinates
        predicted_coordinate_list = [initial_coordinates]

        for i in range(repetitions):

            coordinates = self.one_dynamical_step(sequence_embedded, coordinates, inds_out)

            if self.training and self.args.add_noise_in_training and (i < (repetitions - 1)) and ((i % self.args.noise_in_training_interval) == self.args.noise_in_training_interval - 1):
                coordinates = self.pertrub_coordinates(coordinates)
                coordinates.masked_fill_(inds_out[:, :, None], self.args.aas_coordinate_padding)

            predicted_coordinate_list.append(coordinates)

        # predicted_coordinates = coordinates

        if (true_coordinates is not None) and (aac_measured is not None) and self.training:
            training_loss,lengths = self.training_loss(predicted_coordinate_list[1:], true_coordinates, aac_measured)
            return training_loss,lengths
        else:
            return predicted_coordinate_list

    def predict_dynamics(self,sequence:torch.Tensor,length:torch.Tensor,rep:int=1,initial_coordinates:Optional[torch.Tensor]=None):
        with torch.no_grad():
            intermediate_coordinates = self.fward(sequence,length,initial_coordinates=initial_coordinates)
            for i in range(1, rep):
                intermediate_coordinates += self.fward(sequence,length,initial_coordinates=intermediate_coordinates[-1])[1:]
        return intermediate_coordinates

    def add_embedding_ops(self):
        self.embed = AdaptiveEmbedding(self.args.num_aas+1,self.args.aac_embedding_size,self.args.aac_embedding_size)

    def add_interaction_ops(self):
        in_size = self.args.aac_embedding_size + self.args.num_dimensions
        out_size = self.args.num_dimensions
        self.layer_norm = nn.LayerNorm(in_size)
        if self.args.use_transformer:
            self.interaction = Transformer(in_size,out_size,self.args.num_layers,self.args.num_heads,self.args.model_dim,self.args.query_dim,self.args.inner_dim,self.args.dropout,self.args.dropatt)
        else:
            self.interaction = MlpInteraction(self.args.aac_embedding_size,self.args.mlp_widths,self.args.with_norm)

    def add_convolution_ops(self):
        pass

    def double_data(self, sequence:torch.Tensor, length:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        sequence = sequence.repeat(2,1)
        length = length.repeat(2)
        true_coordinates = true_coordinates.repeat(2,1,1)
        aac_measured = aac_measured.repeat(2,1)

        return sequence,length,true_coordinates, aac_measured

    def get_initial_coordinates(self, sequence:torch.Tensor, length:torch.Tensor, inds_out:torch.Tensor):
        batch_size = sequence.shape[0]

        max_length = sequence.shape[-1]

        # initial_coordinates = (torch.arange(0,max_length,device=device)[None,...].float() - (length[:real_batch_size,:].float()-1)/2)*self.args.initial_distance_between_aas*self.args.coordinates_factor

        # initial_coordinates = F.pad(initial_coordinates[...,None],[0,self.args.num_dimensions-1])

        m = 3.6 # number of aacs per alpha helix turn
        r = 230 # this is the radius of alpha helix, in pm
        h = 150 # this is the translation of each aac along the axis of the alpha helix, in pm
        t = torch.arange(0,max_length,device=sequence.device).float()
        initial_coordinates_x = (r*torch.cos(2*np.pi*t/m))[None,:].expand(batch_size,max_length)
        initial_coordinates_y = (r*torch.sin(2*np.pi*t/m))[None,:].expand(batch_size,max_length)
        initial_coordinates_z = (t[None,...] - (length[:batch_size,None].float()-1)/2)*h

        initial_coordinates = torch.stack((initial_coordinates_x,initial_coordinates_y,initial_coordinates_z),dim=2)*self.args.coordinates_factor

        initial_coordinates.masked_fill_(inds_out[:, :, None], self.args.aas_coordinate_padding)

        return initial_coordinates

    def pertrub_coordinates(self, coordinates:torch.Tensor):
        coordinates =  coordinates + torch.normal(torch.zeros_like(coordinates),self.args.initial_distance_between_aas*self.args.coordinates_factor*self.args.coordinate_perturbation_factor*torch.ones_like(coordinates))
        return coordinates

    def one_dynamical_step(self, sequence_embedded:torch.Tensor, coordinates:torch.Tensor, inds_out:torch.Tensor):
        if self.args.use_transformer:
            all_aacs = torch.cat([sequence_embedded, coordinates], 2)  # [batch,aas,channel]
            # all_aacs_normalized = self.layer_norm(all_aacs)
            interaction_forces = self.interaction(all_aacs,inds_out)
        else:
            interaction_forces = self.interaction(coordinates,sequence_embedded,inds_out)
        # conv_forces = self.conv_layers(all_aacs.transpose(1, 2)).transpose_(1, 2)
        coordinates = coordinates + interaction_forces# + conv_forces
        coordinates.masked_fill_(inds_out[:,:,None],self.args.aas_coordinate_padding)

        return coordinates

    def loss(self, predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        msd = mean_squared_distances(predicted_coordinates, true_coordinates, aac_measured)
        lengths = torch.sum(aac_measured,1,keepdim=True)
        loss = torch.sum(msd*lengths.float())
        loss = loss / ((self.args.initial_distance_between_aas*self.args.coordinates_factor)**2)

        return loss, lengths

    def training_loss(self, coordinates_list:List[torch.Tensor], true_coordinates:torch.Tensor, aac_measured:torch.Tensor):
        aac_measured = aac_measured
        training_loss = 0.0
        if not self.args.use_inermediate_gradients:
            inds = [len(coordinates_list)-1]
        else:
            inds = list(range(self.args.inermediate_gradients_interval-1,len(coordinates_list),self.args.inermediate_gradients_interval))
            if (len(inds)==0) or (inds[-1] != len(coordinates_list)-1):
                inds.append(len(coordinates_list)-1)
            inds = list(reversed(inds))

        for i in inds:
            coeff = 1/(len(coordinates_list)-i)**self.args.inermediate_gradients_exponent
            training_loss_, lengths = self.loss(coordinates_list[i], true_coordinates, aac_measured)
            training_loss = training_loss+coeff*training_loss_
            if self.args.neighboor_dist_coeff is not None:
                training_loss = training_loss+coeff * self.args.neighboor_dist_coeff*self.neighboor_distance_loss(coordinates_list[i], aac_measured)

        training_loss = training_loss + 0.0  # add regularization if needed

        return training_loss,lengths

    def neighboor_distance_loss(self, coordinates:torch.Tensor, aac_measured:torch.Tensor):
        diff = coordinates[:,1:,:]-coordinates[:,:(-1),:]
        squared_dists = torch.sum(diff**2,2)
        squared_dists = squared_dists.view(-1)[aac_measured[...,:(-1)].contiguous().view(-1)]
        loss = torch.sum(squared_dists - (self.args.initial_distance_between_aas * self.args.coordinates_factor)**2)
        loss = loss / ((self.args.initial_distance_between_aas*self.args.coordinates_factor)**2)
        return loss

    def evaluation_loss(self, predicted_coordinates:torch.Tensor, true_coordinates:torch.Tensor, aac_measured:torch.Tensor,):
        loss,lengths = self.loss(predicted_coordinates, true_coordinates, aac_measured)
        return loss/(torch.sum(lengths.float())+self.args.zero_fixer)

    def add_optimizer(self):
        if self.args.optimizer.upper() == 'ADAM':
            self.optimizer = optim.Adam(self.parameters(),self.args.learning_rate)
        elif self.args.optimizer.upper() == 'MOMENTUM':
            self.optimizer = optim.SGD(self.parameters(),self.args.learning_rate,self.args.momentum,nesterov=True)
        else:
            self.optimizer = optim.SGD(self.parameters(), self.args.learning_rate, momentum=0.0, nesterov=True)

        self.scheduler = optim.lr_scheduler.ExponentialLR(self.optimizer,self.args.learning_rate_decay_rate)
        self.learning_rate.data = torch.tensor(self.scheduler.get_lr()[0])

    def number_of_parameters(self):
        return sum([p.numel() for p in self.parameters() if p.requires_grad])

    def do_train_step(self,sequence:torch.Tensor,length:torch.Tensor,true_coordinates:torch.Tensor,aac_measured:torch.Tensor,device='cpu'):

        batch_size = sequence.shape[0]

        self.train()
        self.zero_grad()

        training_loss,lengths = self.fward(sequence,length,true_coordinates=true_coordinates,aac_measured=aac_measured)
        training_loss = torch.sum(training_loss)/(torch.sum(lengths.float())+self.args.zero_fixer)
        training_loss.backward()

        if self.args.use_final_states:
            initial_coordinates = true_coordinates.detach()
            # initial_coordinates = predicted_coordinate_list[-1].detach()
            training_loss2, lengths = self.fward(sequence, length, true_coordinates=true_coordinates,aac_measured=aac_measured,initial_coordinates=initial_coordinates, repetitions=self.args.steps_final_states)
            training_loss2 = torch.sum(training_loss2) / (torch.sum(lengths.float()) + self.args.zero_fixer)
            (training_loss2*self.args.weight_final_states).backward()
        else:
            training_loss2 = None

        self.optimizer.step(None)
        self.steps += 1
        self.examples += batch_size

        return training_loss,training_loss2

    # def do_eval(self,sequence:torch.Tensor,length:torch.Tensor,true_coordinates:torch.Tensor,aac_measured:torch.Tensor,device='cpu'):
    #     self.eval()
    #     with torch.no_grad():
    #         predicted_coordinates = self(sequence,length,mode='EVAL',device=device)[-1]
    #         loss = self.evaluation_loss(predicted_coordinates,true_coordinates,aac_measured)
    #     return loss

    def do_eval(self, data_iterator:BucketIterator, device='cpu'):
        self.eval()

        with torch.no_grad():
            total_loss = 0
            total_len = 0
            for batch in iter(data_iterator):
                sequence, true_coordinates, aac_measured = batch.sequence, batch.coordinates, batch.aac_measured
                sequence = sequence.to(device)
                true_coordinates = true_coordinates.to(device)
                aac_measured = aac_measured.long().to(device)

                true_coordinates = true_coordinates*self.args.coordinates_factor

                length = torch.sum(sequence>0,1)

                total_len += int(sequence.shape[0])

                predicted_coordinates = self.fward(sequence,length)[-1]
                total_loss += self.evaluation_loss(predicted_coordinates,true_coordinates,aac_measured)
            loss = total_loss/total_len

        return loss

    def save_checkpoint(self,filename):
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        torch.save({
            'model_state_dict': self.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict()}
            ,filename)
        logger = logging.getLogger('protein_logger')
        logger.info('saved checkpoint to {:s}'.format(filename))

    def restore_checkpoint(self,filename,also_optimizer=False,device='cpu'):
        checkpoint = torch.load(filename,map_location=device)
        self.load_state_dict(checkpoint['model_state_dict'])
        self.to(device)
        if also_optimizer:
            if self.optimizer is None:
                self.add_optimizer()
            self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.eval()
        logger = logging.getLogger('protein_logger')
        logger.info('restored from file {:s}'.format(filename))

    def replicate_self(self,num_gpus:int):
        if num_gpus > 1:
            replications = nn.DataParallel(self,list(range(num_gpus)),output_device=0)
            self.fward = lambda sequence, length, initial_coordinates=None, repetitions=None, true_coordinates=None, aac_measured=None: replications(sequence, length, initial_coordinates, repetitions, true_coordinates, aac_measured)

    def train_model(self, train_data_iterator:BucketIterator, validation_data_iterator:BucketIterator, dir:Optional[str], device='cpu', max_epochs=None):
        logger = logging.getLogger('protein_logger')

        if self.optimizer is None:
            self.add_optimizer()

        initial_steps, initial_examples = self.steps.clone(), self.examples.clone()

        epochs = float(self.examples) / len(train_data_iterator.dataset)
        initial_epochs = epochs

        first_iteration = True
        first_train_iteration = True
        finished_training = False
        best_loss = np.inf
        try:
            while not finished_training:
                if not first_iteration:
                    self.learning_rate.data = torch.tensor(self.scheduler.get_lr()[0])

                    for batch in iter(train_data_iterator):

                        if max_epochs is not None and epochs > max_epochs:
                            finished_training = True
                            break

                        sequence, true_coordinates, aac_measured = batch.sequence,batch.coordinates,batch.aac_measured
                        sequence = sequence.to(device)
                        true_coordinates = true_coordinates.to(device)
                        aac_measured = aac_measured.long().to(device)
                        length = torch.sum(sequence > 0, 1)

                        true_coordinates = true_coordinates*self.args.coordinates_factor

                        training_loss,training_loss2 = self.do_train_step(sequence, length, true_coordinates, aac_measured,device=device)
                        epochs = float(self.examples) / len(train_data_iterator.dataset)
                        if training_loss2 is None:
                            training_loss2 = -1.0

                        # train step + training eval
                        if (self.steps%self.args.train_eval_frequency == (-1%self.args.train_eval_frequency)) or first_train_iteration:
                            logger.info('steps:{:07d}({:07d}), examples:{:07d}({:07d}), epochs:{:7.2f}({:7.2f}).   training loss={:.5E}.  training loss2={:.5E}.  learning_rate={:.2E}.'.format(self.steps - initial_steps, self.steps, self.examples - initial_examples, self.examples, epochs - initial_epochs, epochs, training_loss,training_loss2,self.learning_rate))

                        first_train_iteration = False
                        self.scheduler.step()

                # eval after epoch
                if (validation_data_iterator is not None) and (first_iteration or (int(epochs)%self.args.test_eval_frequency==0)):

                    loss = self.do_eval(validation_data_iterator, device=device)

                    if first_iteration:
                        logger.info('initial validation loss = {:.5E}'.format(loss))
                    else:
                        logger.info('steps:{:07d}({:07d}), examples:{:07d}({:07d}), epochs:{:7.2f}({:7.2f}),  validation loss = {:.5E}\n'.format(self.steps - initial_steps, self.steps, self.examples - initial_examples, self.examples, epochs - initial_epochs, epochs, loss))

                    # save to checkpoint
                    if (not first_iteration) and (loss < best_loss) and (dir is not None):
                        best_loss = loss
                        self.save_checkpoint(os.path.join(dir, 'checkpoint.ckpt'))

                first_iteration = False

        except Exception as e:
            logger.info('ERROR.  traceback:\n{:s}'.format('      '.join(('\n' + traceback.format_exc().lstrip()).splitlines(True))))
            raise

def init_weight(weight,init_type,initialization_scale):
    if init_type.upper() == 'UNIFORM':
        nn.init.uniform_(weight, -initialization_scale, initialization_scale)
    elif init_type.upper() == 'NORMAL':
        nn.init.normal_(weight, 0.0, initialization_scale)

def init_bias(bias):
    nn.init.constant_(bias, 0.0)

def initialize_weights_transformer(module, init_type, initialization_scale, initialization_scale_proj):
    classname = module.__class__.__name__
    if classname.find('Linear') != -1:
        if hasattr(module, 'weight') and module.weight is not None:
            init_weight(module.weight,init_type,initialization_scale)
        if hasattr(module, 'bias') and module.bias is not None:
            init_bias(module.bias)
    elif classname.find('AdaptiveEmbedding') != -1:
        if hasattr(module, 'embed_proj'):
            if module.embed_proj is not None:
                nn.init.normal_(module.embed_proj, 0.0, initialization_scale_proj)
    elif classname.find('Embedding') != -1:
        if hasattr(module, 'weight'):
            init_weight(module.weight,init_type,initialization_scale)
            module.weight.data[0] = 0
    elif classname.find('LayerNorm') != -1:
        if hasattr(module, 'weight'):
            nn.init.normal_(module.weight, 1.0, 1e-2)
        if hasattr(module, 'bias') and module.bias is not None:
            init_bias(module.bias)
